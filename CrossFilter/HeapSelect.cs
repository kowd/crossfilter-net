﻿using System;

namespace CrossFilter
{
    public static class HeapSelect
    {
        private static T[] By<T, U>(Func<T, U> f, T[] a, int lo, int hi, int k)
            where U : IComparable<U>
        {
            var queue = new T[k = Math.Min(hi - lo, k)];
            U min;
            int i;
            T d;

            for (i = 0; i < k; ++i)
            {
                queue[i] = a[lo++];
            }

            Heap.By(f)(queue, 0, k);

            if (lo < hi && queue.Length > 0)
            {
                min = f(queue[0]);
                do
                {
                    if (f(d = a[lo]).CompareTo(min) > 0)
                    {
                        queue[0] = d;
                        min = f(Heap.By(f)(queue, 0, k)[0]);
                    }
                } while (++lo < hi);
            }

            return queue;
        }

        public static Func<T[], int, int, int, T[]> By<T, U>(Func<T, U> f)
            where U : IComparable<U>
        {
            return (a, lo, hi, k) => By(f, a, lo, hi, k);
        }

        public static T[] It<T>(T[] a, int lo, int hi, int k)
            where T : IComparable<T>
        {
            return By(Identity.It, a, lo, hi, k);
        }
    }
}