﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CrossFilter
{
    public static class Sort
    {
        public static T[] It<T>(T[] a, int lo, int hi)
            where T : IComparable<T>
        {
            var sort = a.Skip(lo).Take(hi - lo).OrderBy(t => t).Select((t, i) => a[lo + i] = t).GetEnumerator();
            while (sort.MoveNext()) {}
            return a;
        }

        public static Func<T[], int, int, T[]> By<T, U>(Func<T, U> f)
            where U : IComparable<U>
        {
            return (a, lo, hi) =>
            {
                var sort = a.Skip(lo).Take(hi - lo).OrderBy(f).Select((t, i) => a[lo + i] = t).GetEnumerator();
                while (sort.MoveNext()) { }
                return a;
            };
        }

        private class Comparer<T, U> : IComparer<T>
            where U : IComparable<U>
        {
            private readonly Func<T, U> _f;

            public Comparer(Func<T, U> f)
            {
                _f = f;
            }

            public int Compare(T x, T y)
            {
                return _f(x).CompareTo(_f(y));
            }
        }
    }
}