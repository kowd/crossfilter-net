﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CrossFilter
{
    public static class XFilter
    {
        public static int[] FilterAll<T>(T[] values)
        {
            return new[] {0, values.Length};
        }

        public static int Zero()
        {
            return 0;
        }

        public static U Null<T, U>(T value)
        {
            return default(U);
        }

        public static Func<T[], int[]> FilterExact<T>(T value)
            where T : IComparable<T>
        {
            return (values) =>
            {
                var n = values.Length;
                return new[]
                {
                    Bisect.Left(values, value, 0, n),
                    Bisect.Right(values, value, 0, n)
                };
            };
        }

        public static Func<T[], int[]> FilterRange<T>(T min, T max)
            where T : IComparable<T>
        {
            return (values) =>
            {
                var n = values.Length;
                return new[]
                {
                    Bisect.Left(values, min, 0, n),
                    Bisect.Left(values, max, 0, n)
                };
            };
        }

        /// <summary>
        /// Returns an array of size n, big enough to store ids up to m.
        /// </summary>
        internal static byte[] Index(int n, int m)
        {
            return m < 0x101
                ? new byte[n]
                : m < 0x10001
                    ? new byte[n] // ushort
                    : new byte[n]; // uint
        }


        // Constructs a new array of size n, with sequential values from 0 to n - 1.
        internal static byte[] Range(int n)
        {
            var range = Index(n, n);
            for (var i = -1; ++i < n;)
            {
                range[i] = (byte) i;
            }

            return range;
        }

        internal static long Capacity(int w)
        {
            return w == 8
                ? 0x100
                : w == 16
                    ? 0x10000
                    : 0x100000000;
        }

        internal static byte[] ArrayLengthen(byte[] array, int length)
        {
            if (array.Length >= length)
            {
                return array;
            }

            // TODO: Return a copy?
            Array.Resize(ref array, length);
            return array;
        }

        internal static byte[] ArrayWiden(byte[] array, int width)
        {
            // TODO: Array widen
            //var copy;
            //switch (width)
            //{
            //    case 16:
            //        copy = crossfilter_array16(array.length);
            //        break;
            //    case 32:
            //        copy = crossfilter_array32(array.length);
            //        break;
            //    default:
            //        throw new Error("invalid array width!");
            //}
            //copy.set(array);
            //return copy;

            return array;
        }

        internal static int ReduceIncrement<G>(int p, G v)
        {
            return p + 1;
        }

        internal static int ReduceDecrement<G>(int p, G v)
        {
            return p - 1;
        }

        internal static Func<int, G, int> ReduceAdd<G>(Func<G, int> f)
        {
            return (p, v) => p + f(v);
        }

        internal static Func<int, G, int> ReduceSubstract<G>(Func<G, int> f)
        {
            return (p, v) => p - f(v);
        }
    }

    public class XFilter<T>
    {
        private T[] data = new T[0]; // the records
        private int n = 0; // the number of records; data.length
        private int m = 0; // a bit mask representing which dimensions are in use
        private int M = 8; // number of dimensions that can fit in `filters`
        private List<byte> filters = new List<byte>(0); // M bits per record; 1 is filtered out
        private List<Action<int, int[], int[]>> filterListeners; // when the filters change
        private List<Action<ICollection<T>, int, int>> dataListeners; // when data is added
        private List<Action<IList<byte>>> removeDataListeners; // when data is removed

        public XFilter(ICollection<T> source = null)
        {
            dataListeners = new List<Action<ICollection<T>, int, int>>();
            removeDataListeners = new List<Action<IList<byte>>>();
            filterListeners = new List<Action<int, int[], int[]>>();

            if (source != null)
            {
                Add(source);
            }
        }

        public Version Version
        {
            get { return default(Version); }
        }

        /// <summary>
        /// Returns the number of records in this crossfilter, irrespective of any filters.
        /// </summary>
        public int Count
        {
            get { return n; }
        }

        /// <summary>
        /// Adds the specified new records to this crossfilter.
        /// </summary>
        public XFilter<T> Add(ICollection<T> newData)
        {
            var n0 = n;
            var n1 = newData.Count;

            // If there's actually new data to add…
            // Merge the new data into the existing data.
            // Lengthen the filter bitset to handle the new records.
            // Notify listeners (dimensions and groups) that new data is available.
            if (n1 > 0)
            {
                Array.Resize(ref data, data.Length + newData.Count);
                Array.Copy(newData.ToArray(), 0, data, n0, newData.Count);
                filters.AddRange(new byte[n1]); // crossfilter_arrayLengthen(filters, n += n1);
                n += n1;
                foreach (var dataListener in dataListeners)
                {
                    dataListener(newData, n0, n1);
                }
            }

            return this;
        }

        /// <summary>
        /// Removes all records that match the current filters.
        /// </summary>
        public void Remove()
        {
            var newIndex = XFilter.Index(n, n);
            var removed = new List<int>();
            int j = 0;
            for (var i = 0; i < n; ++i)
            {
                if (filters[i] != 0)
                {
                    newIndex[i] = (byte) j++;
                }
                else
                {
                    removed.Add(i);
                }
            }

            // Remove all matching records from groups.
            foreach (var filterListener in filterListeners)
            {
                filterListener(0, new int[0], removed.ToArray());
            }

            // Update indexes.
            foreach (var removeDataListener in removeDataListeners)
            {
                removeDataListener(newIndex);
            }

            // Remove old filters and data by overwriting.
            j = 0;
            for (var i = 0; i < n; ++i)
            {
                byte k;
                if ((k = filters[i]) > 0)
                {
                    if (i != j)
                    {
                        filters[j] = k;
                        data[j] = data[i];
                    }

                    ++j;
                }
            }

            Array.Resize(ref data, j);
            while (n > j)
            {
                filters[--n] = 0;
            }
        }

        /// <summary>
        /// Adds a new dimension with the specified value accessor function.
        /// </summary>
        public XDimension<U> Dimension<U>(Func<T, U> value)
            where U : IComparable<U>
        {
            return new XDimension<U>(this, value);
        }

        public class XDimension<U> : IDisposable
            where U : IComparable<U>
        {
            private byte one; // lowest unset bit as mask, e.g., 00001000
            private byte zero; // inverted one, e.g., 11110111
            private U[] values; // sorted, cached array
            private byte[] index; // value rank ↦ object id
            private U[] newValues; // temporary array storing newly-added values
            private byte[] newIndex; // temporary array storing newly-added index
            private Func<byte[], int, int, byte[]> sort; // 
            private Func<U[], int[]> refilter; // for recomputing filter
            private Func<U, int, bool> refilterFunction; // the custom filter function in use
            private List<Action<U[], byte[], int, int>> indexListeners; // when data is added
            private List<IDisposable> dimensionGroups;
            private int lo0 = 0;
            private int hi0 = 0;
            private Func<T, U> value;
            private XFilter<T> _;

            public XDimension(XFilter<T> _, Func<T, U> value)
            {
                indexListeners = new List<Action<U[], byte[], int, int>>();
                dimensionGroups =new List<IDisposable>();

                this._ = _;
                this.value = value;
                one = (byte) (~_.m & -~_.m); // lowest unset bit as mask, e.g., 00001000
                zero = (byte) ~one; // inverted one, e.g., 11110111

                sort = Sort.By<byte, U>(i => newValues[i]);
                refilter = XFilter.FilterAll<U>; // for recomputing filter


                // Updating a dimension is a two-stage process. First, we must update the
                // associated filters for the newly-added records. Once all dimensions have
                // updated their filters, the groups are notified to update.
                _.dataListeners.Insert(0, PreAdd);
                _.dataListeners.Add(PostAdd);

                _.removeDataListeners.Add(RemoveData);

                // Incorporate any existing data into this dimension, and make sure that the
                // filter bitset is wide enough to handle the new dimension.
                _.m |= one;
                if (_.M >= 32 ? one == 0 : (_.m & (1 << _.M) - 1) != 0)
                {
                    // TODO: Array - widen
                    // filters = crossfilter_arrayWiden(filters, M <<= 1);
                }
                PreAdd(_.data, 0, _.n);
                PostAdd(_.data, 0, _.n);
            }

            /// <summary>
            /// Incorporates the specified new records into this dimension.
            /// This function is responsible for updating filters, values, and index.
            /// </summary>
            private void PreAdd(ICollection<T> newData, int n0, int n1)
            {
                // Permute new values into natural order using a sorted index.
                newValues = newData.Select(value).ToArray();
                newIndex = sort(XFilter.Range(n1), 0, n1);
                newValues = Permute.It(newValues, newIndex.Select(ni => (int) ni).ToArray());

                // Bisect newValues to determine which new records are selected.
                var bounds = refilter(newValues);
                var lo1 = bounds[0];
                var hi1 = bounds[1];
                int i;

                if (refilterFunction != null)
                {
                    for (i = 0; i < n1; ++i)
                    {
                        if (!refilterFunction(newValues[i], i))
                        {
                            _.filters[newIndex[i] + n0] |= one;
                        }
                    }
                }
                else
                {
                    for (i = 0; i < lo1; ++i)
                    {
                        _.filters[newIndex[i] + n0] |= one;
                    }

                    for (i = hi1; i < n1; ++i)
                    {
                        _.filters[newIndex[i] + n0] |= one;
                    }
                }

                // If this dimension previously had no data, then we don't need to do the
                // more expensive merge operation; use the new values and index as-is.
                if (n0 == 0)
                {
                    values = newValues;
                    index = newIndex;
                    lo0 = lo1;
                    hi0 = hi1;
                    return;
                }

                var oldValues = values;
                var oldIndex = index;
                var i0 = 0;
                var i1 = 0;

                // Otherwise, create new arrays into which to merge new and old.
                values = new U[_.n];
                index = XFilter.Index(_.n, _.n);

                // Merge the old and new sorted values, and old and new index.
                for (i = 0; i0 < n0 && i1 < n1; ++i)
                {
                    if (oldValues[i0].CompareTo(newValues[i1]) < 0)
                    {
                        values[i] = oldValues[i0];
                        index[i] = oldIndex[i0++];
                    }
                    else
                    {
                        values[i] = newValues[i1];
                        index[i] = (byte) (newIndex[i1++] + n0);
                    }
                }

                // Add any remaining old values.
                for (; i0 < n0; ++i0, ++i)
                {
                    values[i] = oldValues[i0];
                    index[i] = oldIndex[i0];
                }

                // Add any remaining new values.
                for (; i1 < n1; ++i1, ++i)
                {
                    values[i] = newValues[i1];
                    index[i] = (byte) (newIndex[i1] + n0);
                }

                // Bisect again to recompute lo0 and hi0.
                bounds = refilter(values);
                lo0 = bounds[0];
                hi0 = bounds[1];
            }

            /// <summary>
            /// When all filters have updated, notify index listeners of the new values.
            /// </summary>
            private void PostAdd(ICollection<T> newData, int n0, int n1)
            {
                foreach (var l in indexListeners)
                {
                    l(newValues, newIndex, n0, n1);
                }

                newValues = null;
                newIndex = null;
            }

            private void RemoveData(IList<byte> reIndex)
            {
                int j = 0;
                int k;
                for (var i = 0; i < _.n; ++i)
                {
                    if (_.filters[k = index[i]] != 0)
                    {
                        if (i != j)
                        {
                            values[j] = values[i];
                        }

                        index[j] = reIndex[k];
                        ++j;
                    }
                }
                Array.Resize(ref values, j);

                while (j < _.n)
                {
                    index[j++] = 0;
                }

                // Bisect again to recompute lo0 and hi0.
                var bounds = refilter(values);
                lo0 = bounds[0];
                hi0 = bounds[1];
            }

            private XDimension<U> FilterIndexBounds(int[] bounds)
            {
                return FilterIndexBounds(bounds[0], bounds[1]);
            }

            /// <summary>
            /// Updates the selected values based on the specified bounds [lo, hi].
            /// This implementation is used by all the public filter methods.
            /// </summary>
            private XDimension<U> FilterIndexBounds(int lo1, int hi1)
            {
                if (refilterFunction != null)
                {
                    refilterFunction = null;
                    FilterIndexFunction((d, di) => lo1 <= di && di < hi1);
                    lo0 = lo1;
                    hi0 = hi1;
                    return this;
                }

                int i,
                    j,
                    k;
                var added = new List<int>();
                var removed = new List<int>();

                // Fast incremental update based on previous lo index.
                if (lo1 < lo0)
                {
                    for (i = lo1, j = Math.Min(lo0, hi1); i < j; ++i)
                    {
                        _.filters[k = index[i]] ^= one;
                        added.Add(k);
                    }
                }
                else if (lo1 > lo0)
                {
                    for (i = lo0, j = Math.Min(lo1, hi0); i < j; ++i)
                    {
                        _.filters[k = index[i]] ^= one;
                        removed.Add(k);
                    }
                }

                // Fast incremental update based on previous hi index.
                if (hi1 > hi0)
                {
                    for (i = Math.Max(lo1, hi0), j = hi1; i < j; ++i)
                    {
                        _.filters[k = index[i]] ^= one;
                        added.Add(k);
                    }
                }
                else if (hi1 < hi0)
                {
                    for (i = Math.Max(lo0, hi1), j = hi0; i < j; ++i)
                    {
                        _.filters[k = index[i]] ^= one;
                        removed.Add(k);
                    }
                }

                lo0 = lo1;
                hi0 = hi1;
                if (_.filterListeners != null)
                {
                    foreach (var filterListener in _.filterListeners)
                    {
                        filterListener(one, added.ToArray(), removed.ToArray());
                    }
                }

                return this;
            }

            private void FilterIndexFunction(Func<U, int, bool> f)
            {
                int i,
                    k;
                bool x;
                var added = new List<int>();
                var removed = new List<int>();

                for (i = 0; i < _.n; ++i)
                {
                    if ((_.filters[k = index[i]] & one) == 0 ^ (x = f(values[i], i)))
                    {
                        if (x)
                        {
                            _.filters[k] &= zero;
                            added.Add(k);
                        }
                        else
                        {
                            _.filters[k] |= one;
                            removed.Add(k);
                        }
                    }
                }

                if (_.filterListeners != null)
                {
                    foreach (var filterListener in _.filterListeners)
                    {
                        filterListener(one, added.ToArray(), removed.ToArray());
                    }
                }
            }

            public XGroup<U> Group()
            {
                return Group(u => u);
            }

            public XGroup<G> Group<G>(Func<U, G> f)
                where G:IComparable<G>
            {
                return new XGroup<G>(_, this, f);
            }

            /// <summary>
            /// Filters this dimension to select the exact value.
            /// </summary>
            public XDimension<U> FilterExact(U value)
            {
                return FilterIndexBounds((refilter = XFilter.FilterExact(value))(values));
            }

            /// <summary>
            /// Filters this dimension using an arbitrary function.
            /// </summary>
            public XDimension<U> FilterFunction(Func<U, bool> f)
            {
                return FilterFunction((u, i) => f(u));
            }

            /// <summary>
            /// Filters this dimension using an arbitrary function.
            /// </summary>
            public XDimension<U> FilterFunction(Func<U, int, bool> f)
            {
                refilter = XFilter.FilterAll;

                FilterIndexFunction(refilterFunction = f);

                lo0 = 0;
                hi0 = _.n;

                return this;
            }

            /// <summary>
            /// Clears any filters on this dimension.
            /// </summary>
            /// <returns></returns>
            public XDimension<U> FilterAll()
            {
                return FilterIndexBounds((refilter = XFilter.FilterAll)(values));
            }

            /// <summary>
            /// Returns the top K selected records based on this dimension's order. 
            /// Note: observes this dimension's filter, unlike group and groupAll.
            /// </summary>
            public IList<T> Top(int k)
            {
                var array = new List<T>();
                var i = hi0;
                int j;

                while (--i >= lo0 && k > 0)
                {
                    if (_.filters[j = index[i]] == 0)
                    {
                        array.Add(_.data[j]);
                        --k;
                    }
                }

                return array;
            }

            /// <summary>
            /// Returns the bottom K selected records based on this dimension's order.
            /// Note: observes this dimension's filter, unlike group and groupAll.
            /// </summary>
            public IList<T> Bottom(int k)
            {
                var array = new List<T>();
                var i = lo0;
                int j;

                while (i < hi0 && k > 0)
                {
                    if (_.filters[j = index[i]] == 0)
                    {
                        array.Add(_.data[j]);
                        --k;
                    }
                    i++;
                }

                return array;
            }

            /// <summary>
            /// Filters this dimension to select the specified range [lo, hi].
            /// The lower bound is inclusive, and the upper bound is exclusive.
            /// </summary>
            public XDimension<U> FilterRange(U from, U to)
            {
                return FilterIndexBounds((refilter = XFilter.FilterRange(from, to))(values));
            }

            public XDimension<U> Filter(U from, U to)
            {
                return FilterRange(from, to);
            }

            public XDimension<U> Filter(U exact)
            {
                return FilterExact(exact);
            }

            public XDimension<U> Filter(Func<U, bool> f = null)
            {
                return f == null
                    ? FilterAll()
                    : FilterFunction(f);
            }

            public class XGroup<G> : Dictionary<G, int>, IDisposable
                where G: IComparable<G>
            {
                private List<KeyValue<G>> groups; // array of {key, value}
                private byte[] groupIndex; // object id ↦ group id
                private int groupWidth = 8;
                private long groupCapacity;
                private int k = 0; // cardinality
                private Func<KeyValue<G>[], int, int, int, KeyValue<G>[]> select;
                private Func<KeyValue<G>[], int, int, KeyValue<G>[]> heap;
                private Func<int, T, int> reduceAdd;
                private Func<int, T, int> reduceRemove;
                private Func<int> reduceInitial;
                private Action<int, int[], int[]> update;
                private Action reset;
                private bool resetNeeded = true;
                private bool groupAll;
                private XFilter<T> __;
                private Func<U, G> key;
                private XDimension<U> _;

                public XGroup(XFilter<T> __, XDimension<U> _, Func<U, G> key)
                {
                    this.__ = __;
                    this._ = _;
                    this.key = key;
                    groups = new List<KeyValue<G>>();
                    groupCapacity = XFilter.Capacity(groupWidth);
                    update = (a,b,c) => {};
                    reset = () => { };
                    groupAll = key != null && key == XFilter.Null<U, G>;

                    // Ensure that this group will be removed when the dimension is removed.
                    _.dimensionGroups.Add(this);

                    if (key == null)
                    {
                        key = i => (G)(object)i;
                    }

                    // The group listens to the crossfilter for when any dimension changes, so
                    // that it can update the associated reduce values. It must also listen to
                    // the parent dimension for when data is added, and compute new keys.
                    
                    __.filterListeners.Add(update);
                    _.indexListeners.Add(Add);
                    __.removeDataListeners.Add(RemoveData);

                    // Incorporate any existing data into the grouping.
                    Add(_.values, _.index, 0, __.n);

                    ReduceCount().OrderNatural();
                }

                /// <summary>
                /// Incorporates the specified new values into this group.
                /// This function is responsible for updating groups and groupIndex.
                /// </summary>
                private void Add(U[] newValues, byte[] newIndex, int n0, int n1)
                {
                    var oldGroups = groups;
                    var reIndex = XFilter.Index(k, (int) groupCapacity); // TODO: Long capacity?
                    var add = reduceAdd;
                    var initial = reduceInitial;
                    var k0 = k; // old cardinality
                    var i0 = 0; // index of old group
                    var i1 = 0; // index of new record
                    int j; // object id
                    KeyValue<G> g0 = null; // old group
                    G x0 = default(G); // old key
                    G x1 = default(G); // new key
                    KeyValue<G> g; // group to add
                    G x; // key of group to add

                    // If a reset is needed, we don't need to update the reduce values.
                    if (resetNeeded)
                    {
                        add = (a, b) => a;
                        initial = () => default(int);
                    }

                    // Reset the new groups (k is a lower bound).
                    // Also, make sure that groupIndex exists and is long enough.
                    groups = new List<KeyValue<G>>(k);
                    k = 0;
                    groupIndex = k0 > 1
                        ? XFilter.ArrayLengthen(groupIndex, __.n)
                        : XFilter.Index(__.n, (int) groupCapacity);

                    // Get the first old key (x0 of g0), if it exists.
                    if (k0 != 0)
                    {
                        x0 = (g0 = oldGroups[0]).Key;
                    }

                    // Find the first new key (x1), skipping NaN keys.
                    while (i1 < n1 && !((x1 = key(newValues[i1])).CompareTo(x1) >= 0))
                    {
                        ++i1;
                    }

                    // Count the number of added groups,
                    // and widen the group index as needed.
                    Action groupIncrement = () =>
                    {
                        if (++k == groupCapacity)
                        {
                            reIndex = XFilter.ArrayWiden(reIndex, groupWidth <<= 1);
                            groupIndex = XFilter.ArrayWiden(groupIndex, groupWidth);
                            groupCapacity = XFilter.Capacity(groupWidth);
                        }
                    };

                    // While new keys remain…
                    while (i1 < n1)
                    {
                        // Determine the lesser of the two current keys; new and old.
                        // If there are no old keys remaining, then always add the new key.
                        if (g0 != null && x0.CompareTo(x1) <= 0)
                        {
                            g = g0;
                            x = x0;

                            // Record the new index of the old group.
                            reIndex[i0] = (byte) k;

                            // Retrieve the next old key.
                            // TODO: Index out of range exception and not UNDEFINED!
                            if (oldGroups.Count > ++i0 && (g0 = oldGroups[i0]) != null)
                            {
                                x0 = g0.Key;
                            }
                        }
                        else
                        {
                            g = new KeyValue<G>(key: x1, value: initial());
                            x = x1;
                        }

                        // Add the lesser group.
                        groups.Insert(k, g);

                        // Add any selected records belonging to the added group, while
                        // advancing the new key and populating the associated group index.
                        while (!(x1.CompareTo(x) > 0))
                        {
                            groupIndex[j = newIndex[i1] + n0] = (byte) k;
                            if ((__.filters[j] & _.zero) == 0)
                            {
                                g = new KeyValue<G>(g.Key, add(g.Value, __.data[j]));
                            }

                            if (++i1 >= n1)
                            {
                                break;
                            }

                            x1 = key(newValues[i1]);
                        }

                        groupIncrement();
                    }

                    // Add any remaining old groups that were greater than all new keys.
                    // No incremental reduce is needed; these groups have no new records.
                    // Also record the new index of the old group.
                    while (i0 < k0)
                    {
                        groups[reIndex[i0] = (byte) k] = oldGroups[i0++];
                        groupIncrement();
                    }

                    // If we added any new groups before any old groups
                    // update the group index of all the old records.

                    if (k > i0)
                    {
                        for (i0 = 0; i0 < n0; ++i0)
                        {
                            groupIndex[i0] = reIndex[groupIndex[i0]];
                        }
                    }

                    // Modify the update and reset behavior based on the cardinality.
                    // If the cardinality is less than or equal to one, then the groupIndex
                    // is not needed. If the cardinality is zero, then there are no records
                    // and therefore no groups to update or reset. Note that we also must
                    // change the registered listener to point to the new method.
                    j = __.filterListeners.IndexOf(update);
                    if (k > 1)
                    {
                        update = UpdateMany;
                        reset = ResetMany;
                    }
                    else
                    {
                        if (k == 0 && groupAll)
                        {
                            k = 1;
                            groups = new List<KeyValue<G>>()
                            {
                                new KeyValue<G>(default(G), initial())
                            };
                        }
                        if (k == 1)
                        {
                            update = UpdateOne;
                            reset = ResetOne;
                        }
                        else
                        {
                            update = (a, b, c) => { };
                            reset = () => { };
                        }

                        groupIndex = null;
                    }

                    __.filterListeners[j] = update;
                }

                private void RemoveData(IList<byte> index)
                {
                    if (k > 1)
                    {
                        var oldK = k;
                        var oldGroups = groups;
                        var seenGroups = XFilter.Index(oldK, oldK);

                        // Filter out non-matches by copying matching group index entries to
                        // the beginning of the array.
                        var j = 0;
                        for (int i = 0; i < __.n; ++i)
                        {
                            if (__.filters[i] != 0)
                            {
                                seenGroups[groupIndex[j] = groupIndex[i]] = 1;
                                ++j;
                            }
                        }

                        // Reassemble groups including only those groups that were referred
                        // to by matching group index entries.  Note the new group index in
                        // seenGroups.
                        groups = new List<KeyValue<G>>();
                        k = 0;

                        for (var i = 0; i < oldK; ++i)
                        {
                            if (seenGroups[i] != 0)
                            {
                                seenGroups[i] = (byte) k++;
                                groups.Add(oldGroups[i]);
                            }
                        }

                        if (k > 1)
                        {
                            // Reindex the group index using seenGroups to find the new index.
                            for (var i = 0; i < j; ++i)
                            {
                                groupIndex[i] = seenGroups[groupIndex[i]];
                            }
                        }
                        else
                        {
                            groupIndex = null;
                        }

                        var uIndex = __.filterListeners.IndexOf(update);
                        
                        if (k > 1)
                        {
                            reset = ResetMany;
                            update = UpdateMany;
                        }
                        else if (k == 1)
                        {
                            reset = ResetOne;
                            update = UpdateOne;
                        }
                        else
                        {
                            reset = () => { };
                            update = (a, b, c) => { };
                        }

                        __.filterListeners[uIndex] = update;

                    }
                    else if (k == 1)
                    {
                        if (groupAll)
                        {
                            return;
                        }
                        for (var i = 0; i < __.n; ++i)
                        {
                            if (__.filters[i] != 0)
                            {
                                return;
                            }
                        }
                        groups = new List<KeyValue<G>>();
                        k = 0;
                        __.filterListeners[__.filterListeners.IndexOf(update)] = (a, b, c) => { };
                        reset = () => { };
                    }
                }

                /// <summary>
                ///  Reduces the specified selected or deselected records. 
                /// This function is only used when the cardinality is greater than 1.
                /// </summary>
                private void UpdateMany(int filterOne, int[] added, int[] removed)
                {
                    if (filterOne == _.one || resetNeeded) return;

                    int i,
                        k,
                        n;
                    KeyValue<G> g;

                    // Add the added values.
                    for (i = 0, n = added.Length; i < n; ++i)
                    {
                        if ((__.filters[k = added[i]] & _.zero) == 0)
                        {
                            g = groups[groupIndex[k]];
                            g.Value = reduceAdd(g.Value, __.data[k]);
                        }
                    }

                    // Remove the removed values.
                    for (i = 0, n = removed.Length; i < n; ++i)
                    {
                        if ((__.filters[k = removed[i]] & _.zero) == filterOne)
                        {
                            g = groups[groupIndex[k]];
                            g.Value = reduceRemove(g.Value, __.data[k]);
                        }
                    }
                }

                /// <summary>
                /// Reduces the specified selected or deselected records. 
                /// This function is only used when the cardinality is 1.
                /// </summary>
                private void UpdateOne(int filterOne, int[] added, int[] removed)
                {
                    if (filterOne == _.one || resetNeeded)
                    {
                        return;

                    }

                    int i,
                        k,
                        n;
                    var g = groups[0];

                    // Add the added values.
                    for (i = 0, n = added.Length; i < n; ++i)
                    {
                        if ((__.filters[k = added[i]] & _.zero) == 0)
                        {
                            g.Value = reduceAdd(g.Value, __.data[k]);
                        }
                    }

                    // Remove the removed values.
                    for (i = 0, n = removed.Length; i < n; ++i)
                    {
                        if ((__.filters[k = removed[i]] & _.zero) == filterOne)
                        {
                            g.Value = reduceRemove(g.Value, __.data[k]);
                        }
                    }
                }

                /// <summary>
                /// Recomputes the group reduce values from scratch.
                /// This function is only used when the cardinality is greater than 1.
                /// </summary>
                private void ResetMany()
                {
                    int i;
                    KeyValue<G> g;

                    // Reset all group values.
                    for (i = 0; i < k; ++i)
                    {
                        groups[i].Value = reduceInitial();
                    }

                    // Add any selected records.
                    for (i = 0; i < __.n; ++i)
                    {
                        if ((__.filters[i] & _.zero) == 0)
                        {
                            g = groups[groupIndex[i]];
                            g.Value = reduceAdd(g.Value, __.data[i]);
                        }
                    }
                }

                /// <summary>
                /// Recomputes the group reduce values from scratch. This function is only used when the cardinality is 1.
                /// </summary>
                private void ResetOne()
                {
                    int i;
                    var g = groups[0];

                    // Reset the singleton group values.
                    g.Value = reduceInitial();

                    // Add any selected records.
                    for (i = 0; i < __.n; ++i)
                    {
                        if ((__.filters[i] & _.zero) == 0)
                        {
                            g.Value = reduceAdd(g.Value, __.data[i]);
                        }
                    }
                }

                /// <summary>
                /// Returns the array of group values, in the dimension's natural order.
                /// </summary>
                public KeyValue<G>[] All()
                {
                    if (resetNeeded)
                    {
                        reset();
                        resetNeeded = false;
                    }

                    return groups.ToArray();
                }

                /// <summary>
                /// Returns a new array containing the top K group values, in reduce order.
                /// </summary>
                public KeyValue<G>[] Top(int k)
                {
                    var top = select(All(), 0, groups.Count, k);
                    return heap(top, 0, top.Length);
                }

                /// <summary>
                /// Sets the reduce behavior for this group to use the specified functions.
                /// This method lazily recomputes the reduce values, waiting until needed.
                /// </summary>
                public XGroup<G> Reduce(Func<int, T, int> add, Func<int, T, int> remove, Func<int> initial)
                {
                    reduceAdd = add;
                    reduceRemove = remove;
                    reduceInitial = initial;
                    resetNeeded = true;
                    return this;
                }

                /// <summary>
                /// A convenience method for reducing by count.
                /// </summary>
                public XGroup<G> ReduceCount()
                {
                    return Reduce(XFilter.ReduceIncrement, XFilter.ReduceDecrement, XFilter.Zero);
                }

                /// <summary>
                /// A convenience method for reducing by sum(value).
                /// </summary>
                public XGroup<G> ReduceSum(Func<T, int> value)
                {
                    return Reduce(XFilter.ReduceAdd(value), XFilter.ReduceSubstract(value), XFilter.Zero);
                }

                /// <summary>
                /// Sets the reduce order, using the specified accessor.
                /// </summary>
                public XGroup<G> Order<O>(Func<G, O> value)
                    where O : IComparable<O>
                {
                    Func<KeyValue<G>, int> valueOf = kv => kv.Value;
                    select = HeapSelect.By(valueOf);
                    heap = Heap.SortBy(valueOf);
                    return this;
                }

                /// <summary>
                ///  A convenience method for natural ordering by reduce value.
                /// </summary>
                public XGroup<G> OrderNatural()
                {
                    return Order(Identity.It);
                }

                /// <summary>
                /// Returns the cardinality of this group, irrespective of any filters.
                /// </summary>
                public int Count
                {
                    get { return k; }
                }
                
                public void Dispose()
                {
                    var i = __.filterListeners.IndexOf(update);
                    if (i >= 0)
                    {
                        __.filterListeners.RemoveAt(i);
                    }

                    i = _.indexListeners.IndexOf(Add);
                    if (i >= 0)
                    {
                        _.indexListeners.RemoveAt(i);
                    }

                    i = __.removeDataListeners.IndexOf(RemoveData);
                    if (i >= 0)
                    {
                        __.removeDataListeners.RemoveAt(i);
                    }
                }

                
            }

            /// <summary>
            /// A convenience function for generating a singleton group.
            /// </summary>
            public SingletonGroup<U> GroupAll()
            {
                return new SingletonGroup<U>(Group(XFilter.Null<U, U>));
            }

            public class SingletonGroup<U> : IDisposable
                where U: IComparable<U>
            {
                private readonly XGroup<U> _group;

                public SingletonGroup(XGroup<U> group)
                {
                    _group = @group;
                }

                public SingletonGroup<U> Reduce(Func<int, T, int> add, Func<int, T, int> remove, Func<int> initial)
                {
                    _group.Reduce(add, remove, initial);
                    return this;
                }

                public SingletonGroup<U> ReduceCount()
                {
                    _group.ReduceCount();
                    return this;
                }

                public SingletonGroup<U> ReduceSum(Func<T, int> value)
                {
                    _group.ReduceSum(value);
                    return this;
                }

                public int Value
                {
                    get { return _group.All()[0].Value; }
                }

                public void Dispose()
                {
                    _group.Dispose();
                }
            }

            /// <summary>
            /// Removes this dimension and associated groups and event listeners.
            /// </summary>
            public void Dispose()
            {
                foreach (var @group in dimensionGroups)
                {
                    @group.Dispose();
                }

                var i = _.dataListeners.IndexOf(PreAdd);
                if (i >= 0)
                {
                    _.dataListeners.RemoveAt(i);
                }

                i = _.dataListeners.IndexOf(PostAdd);
                if (i >= 0)
                {
                    _.dataListeners.RemoveAt(i);
                }
                
                i = _.removeDataListeners.IndexOf(RemoveData);
                if (i >= 0)
                {
                    _.removeDataListeners.RemoveAt(i);
                }
                _.m &= zero;

                FilterAll();
            }
        }

        /// <summary>
        /// A convenience method for groupAll on a dummy dimension.
        /// This implementation can be optimized since it always has cardinality 1.
        /// </summary>
        public SingletonGroup GroupAll()
        {
            return new SingletonGroup(this);
        }

        public class SingletonGroup : IDisposable
        {
            private Func<int, T, int> reduceAdd;
            private Func<int, T, int> reduceRemove;
            private Func<int> reduceInitial;
            private int reduceValue;
            private bool resetNeeded = true;
            private XFilter<T> _;

            public SingletonGroup(XFilter<T> _)
            {
                this._ = _;

                // The group listens to the crossfilter for when any dimension changes, so
                // that it can update the reduce value. It must also listen to the parent
                // dimension for when data is added.
                _.filterListeners.Add(Update);
                _.dataListeners.Add(Add);

                // For consistency; actually a no-op since resetNeeded is true.
                Add(_.data, 0, _.n);

                ReduceCount();
            }

            /// <summary>
            /// Incorporates the specified new values into this group.
            /// </summary>
            private void Add(ICollection<T> newData, int n0, int n1)
            {
                int i;

                if (resetNeeded) return;

                // Add the added values.
                for (i = n0; i < _.n; ++i)
                {
                    if (_.filters[i] == 0)
                    {
                        reduceValue = reduceAdd(reduceValue, _.data[i]);
                    }
                }
            }

            // Reduces the specified selected or deselected records.
            private void Update(int filterOne, int[] added, int[] removed)
            {
                int i,
                    k,
                    n;

                if (resetNeeded) return;

                // Add the added values.
                for (i = 0, n = added.Length; i < n; ++i)
                {
                    if (_.filters[k = added[i]] == 0)
                    {
                        reduceValue = reduceAdd(reduceValue, _.data[k]);
                    }
                }

                // Remove the removed values.
                for (i = 0, n = removed.Length; i < n; ++i)
                {
                    if (_.filters[k = removed[i]] == filterOne)
                    {
                        reduceValue = reduceRemove(reduceValue, _.data[k]);
                    }
                }
            }

            /// <summary>
            /// Recomputes the group reduce value from scratch.
            /// </summary>
            private void Reset()
            {
                int i;

                reduceValue = reduceInitial();

                for (i = 0; i < _.n; ++i)
                {
                    if (_.filters[i] == 0)
                    {
                        reduceValue = reduceAdd(reduceValue, _.data[i]);
                    }
                }
            }

            /// <summary>
            /// Sets the reduce behavior for this group to use the specified functions.
            /// This method lazily recomputes the reduce value, waiting until needed.
            /// </summary>
            public SingletonGroup Reduce(Func<int, T, int> add, Func<int, T, int> remove, Func<int> initial)
            {
                reduceAdd = add;
                reduceRemove = remove;
                reduceInitial = initial;
                resetNeeded = true;
                return this;
            }

            /// <summary>
            /// A convenience method for reducing by count.
            /// </summary>
            public SingletonGroup ReduceCount()
            {
                return Reduce(XFilter.ReduceIncrement, XFilter.ReduceDecrement, XFilter.Zero);
            }

            /// <summary>
            /// A convenience method for reducing by sum(value).
            /// </summary>
            public SingletonGroup ReduceSum(Func<T, int> value)
            {
                return Reduce(XFilter.ReduceAdd(value), XFilter.ReduceSubstract(value), XFilter.Zero);
            }

            public int Value
            {
                get
                {
                    if (resetNeeded)
                    {
                        Reset();
                        resetNeeded = false;
                    }

                    return reduceValue;
                }
            }

            /// <summary>
            /// Removes this group and associated event listeners.
            /// </summary>
            public void Dispose()
            {
                var i = _.filterListeners.IndexOf(Update);
                if (i >= 0)
                {
                    _.filterListeners.RemoveAt(i);
                }

                i = _.dataListeners.IndexOf(Add);
                if (i >= 0)
                {
                    _.dataListeners.RemoveAt(i);
                }
            }
        }
    }
}