using System.Diagnostics;

namespace CrossFilter
{
    public class KeyValue<T>
    {
        [DebuggerStepThrough]
        public KeyValue(T key, int value)
        {
            Key = key;
            Value = value;
        }

        public T Key;
        public int Value;

        public override bool Equals(object obj)
        {
            var other = obj as KeyValue<T>;
            return other != null
                   && other.Value == Value
                   && Equals(other.Key, Key);
        }

        public override int GetHashCode()
        {
            return Key != null
                ? 12123 ^ Key.GetHashCode() ^ Value
                : 12123 ^ Value;
        }

        public override string ToString()
        {
            return string.Format("Key:{0}, Value:{1}", Key, Value);
        }
    }
}