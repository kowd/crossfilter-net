﻿namespace CrossFilter
{
    public static class Permute
    {
        public static T[] It<T>(T[] array, int[] indexes)
        {
            var n = indexes.Length;
            var copy = new T[n];
            for (int i = 0; i < n; ++i)
            {
                var index = indexes[i];
                copy[i] = index >= 0 && index < array.Length
                    ? array[index]
                    : default(T);
            }

            return copy;
        }
    }
}