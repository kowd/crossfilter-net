﻿using System;

namespace CrossFilter
{
    public static class Bisect
    {
        /// <summary>
        /// Locate the insertion point for x in a to maintain sorted order. The
        /// arguments lo and hi may be used to specify a subset of the array which
        /// should be considered; by default the entire array is used. If x is already
        /// present in a, the insertion point will be before (to the left of) any
        /// existing entries. The return value is suitable for use as the first
        /// argument to `array.splice` assuming that a is already sorted.
        ///
        /// The returned insertion point i partitions the array a into two halves so
        /// that all v &lt; x for v in a[lo:i] for the left side and all v >= x for v in
        /// a[i:hi] for the right side.
        /// </summary>
        private static int LeftBy<T, U>(Func<T, U> f, T[] a, U x, int lo, int hi)
            where U : IComparable<U>
        {
            while (lo < hi)
            {
                var mid = (lo + hi) >> 1;
                if (f(a[mid]).CompareTo(x) < 0)
                {
                    lo = mid + 1;
                }
                else
                {
                    hi = mid;
                }
            }
            return lo;
        }

        public static Func<T[], U, int, int, int> LeftBy<T, U>(Func<T, U> f)
            where U : IComparable<U>
        {
            return (a, x, lo, hi) => LeftBy(f, a, x, lo, hi);
        }

        public static int Left<T>(T[] a, T x, int lo, int hi)
            where T : IComparable<T>
        {
            return LeftBy(Identity.It, a, x, lo, hi);
        }

        /// <summary>
        /// Similar to bisectLeft, but returns an insertion point which comes after (to
        /// the right of) any existing entries of x in a.
        ///
        /// The returned insertion point i partitions the array into two halves so that
        /// all v &lt;= x for v in a[lo:i] for the left side and all v > x for v in
        /// a[i:hi] for the right side.
        /// </summary>
        private static int RightBy<T, U>(Func<T, U> f, T[] a, U x, int lo, int hi)
            where U : IComparable<U>
        {
            while (lo < hi)
            {
                var mid = (lo + hi) >> 1;
                if (x.CompareTo(f(a[mid])) < 0)
                {
                    hi = mid;
                }
                else
                {
                    lo = mid + 1;
                }
            }
            return lo;
        }

        public static Func<T[], U, int, int, int> RightBy<T, U>(Func<T, U> f)
            where U : IComparable<U>
        {
            return (a, x, lo, hi) => RightBy(f, a, x, lo, hi);
        }

        public static int Right<T>(T[] a, T x, int lo, int hi)
            where T : IComparable<T>
        {
            return RightBy(Identity.It, a, x, lo, hi);
        }

        public static Func<T[], U, int, int, int> By<T, U>(Func<T, U> f)
            where U : IComparable<U>
        {
            return RightBy(f);
        }

        public static int It<T>(T[] a, T x, int lo, int hi)
            where T : IComparable<T>
        {
            return Right(a, x, lo, hi);
        }
    }
}