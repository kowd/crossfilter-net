﻿using System;

namespace CrossFilter
{
    public static class Heap
    {
        /// <summary>
        /// Builds a binary heap within the specified array a[lo:hi]. The heap has the
        /// property such that the parent a[lo+i] is always less than or equal to its
        /// two children: a[lo+2*i+1] and a[lo+2*i+2].
        /// </summary>
        private static T[] By<T, U>(Func<T, U> f, T[] a, int lo, int hi)
            where U : IComparable<U>

        {
            var n = hi - lo;
            var i = (n >> 1) + 1;
            while (--i > 0)
            {
                Sift(f, a, i, n, lo);
            }

            return a;
        }

        public static Func<T[] , int, int, T[]> By<T, U>(Func<T, U> f)
            where U : IComparable<U>
        {
            return (a, lo, hi) => By(f, a, lo, hi);
        }

        /// <summary>
        /// Sorts the specified array a[lo:hi] in descending order, assuming it is
        /// already a heap.
        /// </summary>
        private static T[] SortBy<T, U>(Func<T, U> f, T[] a, int lo, int hi)
            where U : IComparable<U>
        {
            var n = hi - lo;
            T t;
            while (--n > 0)
            {
                t = a[lo];
                a[lo] = a[lo + n];
                a[lo + n] = t;
                Sift(f, a, 1, n, lo);
            }

            return a;
        }

        public static Func<T[], int, int, T[]> SortBy<T, U>(Func<T, U> f)
            where U : IComparable<U>
        {
            return (a, lo, hi) => SortBy(f, a, lo, hi);
        }

        public static T[] It<T>(T[] a, int lo, int hi)
            where T : IComparable<T>
        {
            return By(Identity.It, a, lo, hi);
        }

        public static T[] Sort<T>(T[] a, int lo, int hi)
            where T : IComparable<T>
        {
            return SortBy(Identity.It, a, lo, hi);
        }

        /// <summary>
        /// Sifts the element a[lo+i-1] down the heap, where the heap is the contiguous
        /// slice of array a[lo:lo+n]. This method can also be used to update the heap
        /// incrementally, without incurring the full cost of reconstructing the heap.
        /// </summary>
        private static void Sift<T, U>(Func<T, U> f, T[] a, int i, int n, int lo)
            where U : IComparable<U>
        {
            var d = a[--lo + i];
            var x = f(d);
            int child;
            while ((child = i << 1) <= n)
            {
                if (child < n && f(a[lo + child]).CompareTo(f(a[lo + child + 1])) > 0)
                {
                    child++;
                }
                if (x.CompareTo(f(a[lo + child])) <= 0)
                {
                    break;
                }

                a[lo + i] = a[lo + child];
                i = child;
            }

            a[lo + i] = d;
        }
    }
}