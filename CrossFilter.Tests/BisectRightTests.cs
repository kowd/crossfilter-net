using System;
using System.Linq;
using NUnit.Framework;

namespace CrossFilter.Tests
{
    [TestFixture]
    public class BisectRightTests
    {
        [Test]
        public void FindsTheIndexAfterAnExactMatch()
        {
            var array = new[] {1, 2, 3};
            var obj = array.Select(Tuple.Create<int>).ToArray();

            Assert.AreEqual(Bisect.Right(array, 1, 0, 3), 1);
            Assert.AreEqual(Bisect.Right(array, 2, 0, 3), 2);
            Assert.AreEqual(Bisect.Right(array, 3, 0, 3), 3);

            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 1, 0, 3), 1);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 2, 0, 3), 2);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 3, 0, 3), 3);
        }

        [Test]
        public void FindsTheIndexAfterTheLastMatch()
        {
            var array = new[] {1, 2, 2, 3};
            var obj = array.Select(i => Tuple.Create(i)).ToArray();

            Assert.AreEqual(Bisect.Right(array, 1, 0, 4), 1);
            Assert.AreEqual(Bisect.Right(array, 2, 0, 4), 3);
            Assert.AreEqual(Bisect.Right(array, 3, 0, 4), 4);

            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 1, 0, 4), 1);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 2, 0, 4), 3);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 3, 0, 4), 4);
        }

        [Test]
        public void FindsTheInsertionPointOfANonExactMatch()
        {
            var array = new double[] {1, 2, 3};
            var obj = array.Select(i => Tuple.Create(i)).ToArray();

            Assert.AreEqual(Bisect.Right(array, 0.5, 0, 3), 0);
            Assert.AreEqual(Bisect.Right(array, 1.5, 0, 3), 1);
            Assert.AreEqual(Bisect.Right(array, 2.5, 0, 3), 2);
            Assert.AreEqual(Bisect.Right(array, 3.5, 0, 3), 3);

            Assert.AreEqual(Bisect.RightBy<Tuple<double>, double>(t => t.Item1)(obj, 0.5, 0, 3), 0);
            Assert.AreEqual(Bisect.RightBy<Tuple<double>, double>(t => t.Item1)(obj, 1.5, 0, 3), 1);
            Assert.AreEqual(Bisect.RightBy<Tuple<double>, double>(t => t.Item1)(obj, 2.5, 0, 3), 2);
            Assert.AreEqual(Bisect.RightBy<Tuple<double>, double>(t => t.Item1)(obj, 3.5, 0, 3), 3);
        }

        [Test]
        public void ObservesTheLowerBound()
        {
            var array = new[] {1, 2, 3, 4, 5};
            var obj = array.Select(i => Tuple.Create(i)).ToArray();

            Assert.AreEqual(Bisect.Right(array, 0, 2, 5), 2);
            Assert.AreEqual(Bisect.Right(array, 1, 2, 5), 2);
            Assert.AreEqual(Bisect.Right(array, 2, 2, 5), 2);
            Assert.AreEqual(Bisect.Right(array, 3, 2, 5), 3);
            Assert.AreEqual(Bisect.Right(array, 4, 2, 5), 4);
            Assert.AreEqual(Bisect.Right(array, 5, 2, 5), 5);
            Assert.AreEqual(Bisect.Right(array, 6, 2, 5), 5);

            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 0, 2, 5), 2);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 1, 2, 5), 2);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 2, 2, 5), 2);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 3, 2, 5), 3);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 4, 2, 5), 4);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 5, 2, 5), 5);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 6, 2, 5), 5);
        }

        [Test]
        public void ObservesTheLowerAndUpperBounds()
        {
            var array = new[] {1, 2, 3, 4, 5};
            var obj = array.Select(i => Tuple.Create(i)).ToArray();

            Assert.AreEqual(Bisect.Right(array, 0, 2, 3), 2);
            Assert.AreEqual(Bisect.Right(array, 1, 2, 3), 2);
            Assert.AreEqual(Bisect.Right(array, 2, 2, 3), 2);
            Assert.AreEqual(Bisect.Right(array, 3, 2, 3), 3);
            Assert.AreEqual(Bisect.Right(array, 4, 2, 3), 3);
            Assert.AreEqual(Bisect.Right(array, 5, 2, 3), 3);
            Assert.AreEqual(Bisect.Right(array, 6, 2, 3), 3);

            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 0, 2, 3), 2);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 1, 2, 3), 2);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 2, 2, 3), 2);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 3, 2, 3), 3);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 4, 2, 3), 3);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 5, 2, 3), 3);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 6, 2, 3), 3);
        }

        [Test]
        public void LargeArrays()
        {
            var array = new int[1 << 12];
            var i = array.Length;
            array[i - 5] = 1;
            array[i - 4] = 2;
            array[i - 3] = 3;
            array[i - 2] = 4;
            array[i - 1] = 5;
            var obj = array.Select(n => Tuple.Create(n)).ToArray();

            Assert.AreEqual(Bisect.Right(array, 0, i - 5, i), i - 5);
            Assert.AreEqual(Bisect.Right(array, 1, i - 5, i), i - 4);
            Assert.AreEqual(Bisect.Right(array, 2, i - 5, i), i - 3);
            Assert.AreEqual(Bisect.Right(array, 3, i - 5, i), i - 2);
            Assert.AreEqual(Bisect.Right(array, 4, i - 5, i), i - 1);
            Assert.AreEqual(Bisect.Right(array, 5, i - 5, i), i - 0);
            Assert.AreEqual(Bisect.Right(array, 6, i - 5, i), i - 0);

            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 0, i - 5, i), i - 5);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 1, i - 5, i), i - 4);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 2, i - 5, i), i - 3);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 3, i - 5, i), i - 2);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 4, i - 5, i), i - 1);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 5, i - 5, i), i - 0);
            Assert.AreEqual(Bisect.RightBy<Tuple<int>, int>(t => t.Item1)(obj, 6, i - 5, i), i - 0);
        }
    }
}