﻿using System;
using System.Linq;
using NUnit.Framework;

namespace CrossFilter.Tests
{
    [TestFixture]
    public class HeapSelectTests
    {
        [Test]
        public void CanSelectFromASmallArrayOfPositiveIntegers()
        {
            var array = new []{6, 5, 3, 1, 8, 7, 2, 4};
            CollectionAssert.AreEqual(HeapSelect.It(array, 0, array.Length, 1), new []{8});
            CollectionAssert.AreEqual(HeapSelect.It(array, 0, array.Length, 2).OrderByDescending(i => i).ToArray(), new[] {8, 7});
            CollectionAssert.AreEqual(HeapSelect.It(array, 0, array.Length, 3).OrderByDescending(i => i).ToArray(), new[] {8, 7, 6});
            CollectionAssert.AreEqual(HeapSelect.It(array, 0, array.Length, 4).OrderByDescending(i => i).ToArray(), new[] {8, 7, 6, 5});
            CollectionAssert.AreEqual(HeapSelect.It(array, 0, array.Length, 5).OrderByDescending(i => i).ToArray(), new[] {8, 7, 6, 5, 4});
            CollectionAssert.AreEqual(HeapSelect.It(array, 0, array.Length, 6).OrderByDescending(i => i).ToArray(), new[] {8, 7, 6, 5, 4, 3});
            CollectionAssert.AreEqual(HeapSelect.It(array, 0, array.Length, 7).OrderByDescending(i => i).ToArray(), new[] {8, 7, 6, 5, 4, 3, 2});
            CollectionAssert.AreEqual(HeapSelect.It(array, 0, array.Length, 8).OrderByDescending(i => i).ToArray(), new[] {8, 7, 6, 5, 4, 3, 2, 1});
        }

        [Test]
        public void DoesNotAffectTheOriginalOrderReturnsACopy()
        {
            var array = new []{6, 5, 3, 1, 8, 7, 2, 4};
            HeapSelect.It(array, 0, array.Length, 4);
            CollectionAssert.AreEqual(array, new[] {6, 5, 3, 1, 8, 7, 2, 4});
        }

        [Test]
        public void ReturnsFewerThanKElementsWhenKIsTooBig()
        {
            var array = new[] {6, 5, 3, 1, 8, 7, 2, 4};
            CollectionAssert.AreEqual(HeapSelect.It(array, 0, array.Length, 8).OrderByDescending(i => i).ToArray(), new []{8, 7, 6, 5, 4, 3, 2, 1});
        }

        [Test]
        public void ReturnsAnEmptyArrayWhenSelectingNothing()
        {
            var array = new Tuple<int>[0];
            CollectionAssert.AreEqual(HeapSelect.By<Tuple<int>,int>(s => s.Item1)(array, 0, array.Length, 1), new Tuple<int>[0]);
        }

        [Test]
        public void TheReturnedArrayIsABinaryHeap()
        {
            var array = new[] {6, 5, 3, 1, 8, 7, 2, 4};
            for (var i = 0; i < 10; ++i)
            {
                Assert.IsTrue(Heapy(HeapSelect.It(array, 0, array.Length, i)), array + "");
            }
        }

        private bool Heapy<T>(T[] array)
            where T:IComparable<T>
        {
            var n = array.Length;
            for (var i = 1; i < n; ++i)
            {
                if (array[i].CompareTo(array[i - 1 >> 1]) < 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}