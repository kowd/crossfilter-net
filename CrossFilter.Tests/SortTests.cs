﻿using System;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;

namespace CrossFilter.Tests
{
    [TestFixture]
    public class SortTests 
    {
        [Test]
        [Category("Integration")]
        public void CanSortALargeishUint32ArrayQuickly()
        {
            var n = (int)1e6;
            var typedArray = new uint[n];
            var watch = new Stopwatch();
            watch.Start();
            var random = new Random();
            for (var i = 0; i < n; i++) typedArray[i] = (uint)random.Next(0, int.MaxValue);

            Sort.It(typedArray, 0, n);
            var duration = watch.ElapsedMilliseconds;
            Assert.Greater(600, duration);
            for (var i = 1; i < n; i++) Assert.IsTrue(typedArray[i - 1] <= typedArray[i]);
        }
    
        [Test]
        public void CanSortASmallArrayOfPositiveIntegers()
        {
            var array = new[] {6, 5, 3, 1, 8, 7, 2, 4};
            Assert.AreSame(Sort.It(array, 0, array.Length), array);
            CollectionAssert.AreEqual(array, new[] {1, 2, 3, 4, 5, 6, 7, 8});
        }


        [Test]
        public void CanSortASubsetOfASmallArrayOfPositiveIntegers()
        {
            var array = new[] {6, 5, 3, 1, 8, 7, 2, 4};
            Assert.AreSame(Sort.It(array, 0, 4), array);
            CollectionAssert.AreEqual(array, new[] {1, 3, 5, 6, 8, 7, 2, 4});
            Assert.AreSame(Sort.It(array, 4, 8), array);
            CollectionAssert.AreEqual(array, new[] {1, 3, 5, 6, 2, 4, 7, 8});
        }

        [Test]
        public void CanSortASmallArrayOfStrings()
        {
            var array = new[] {"1", "2", "10"};
            Assert.AreSame(Sort.It(array, 0, array.Length), array);
            CollectionAssert.AreEqual(array, new[] {"1", "10", "2"});
        }

        [Test]
        public void CanSortASmallArrayOfObjectsUsingAnAccessor()
        {
            var array = new[] {new Tuple<int>(6), new Tuple<int>(1), new Tuple<int>(3), new Tuple<int>(8)};
            Assert.AreSame(Sort.By<Tuple<int>, int>(d => d.Item1)(array, 0, array.Length), array);
            CollectionAssert.AreEqual(array, new[] {new Tuple<int>(1), new Tuple<int>(3), new Tuple<int>(6), new Tuple<int>(8)});
        }

        [Test]
        public void CanSortASmallInt32Array()
        {
            var n = 17;
            var typedArray = new int[n];
            var untypedArray = new object[n];
            var untypedActual = new object[n];
            var random = new Random();
            for (var i = 0; i < n; i++)
            {
                untypedArray[i] = (typedArray[i] = random.Next());
            }

            Assert.AreSame(Sort.It(typedArray, 0, n), typedArray);
            var sorted = untypedArray.OrderBy(i => (int) i).ToArray();
            for (var i = 0; i < n; i++)
            {
                untypedActual[i] = typedArray[i];
            }
            CollectionAssert.AreEqual(untypedActual, sorted);
        }

        [Test]
        public void CanSortASmallishFloat64Array()
        {
            var n = 10000;
            var typedArray = new double[n];
            var untypedArray = new object[n];
            var untypedActual = new object[n];
            var random = new Random();
            for (var i = 0; i < n; i++)
            {
                untypedArray[i] = (typedArray[i] = random.NextDouble());
            }

            Assert.AreSame(Sort.It(typedArray, 0, n), typedArray);
            var sorted = untypedArray.OrderBy(i => (double) i).ToArray();
            for (var i = 0; i < n; i++)
            {
                untypedActual[i] = typedArray[i];
            }
            CollectionAssert.AreEqual(untypedActual, sorted);
        }
    }
}