using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace CrossFilter.Tests
{
    [TestFixture]
    public class DimensionTests : XFilterTests
    {
        [Test]
        public void Top_RetursTheTopKRecordsByValueInDescendingOrder()
        {
            CollectionAssert.AreEqual(_total.Top(3), new[]
            {
                new Order(date: "2011-11-14T16:28:54Z", quantity: 1, total: 300, tip: 200, type: "visa"),
                new Order(date: "2011-11-14T20:49:07Z", quantity: 2, total: 290, tip: 200, type: "tab"),
                new Order(date: "2011-11-14T21:18:48Z", quantity: 4, total: 270, tip: 0, type: "tab")
            });

            CollectionAssert.AreEqual(_date.Top(3), new[]
            {
                new Order(date: "2011-11-14T23:28:54Z", quantity: 2, total: 190, tip: 100, type: "tab"),
                new Order(date: "2011-11-14T23:23:29Z", quantity: 2, total: 190, tip: 100, type: "tab"),
                new Order(date: "2011-11-14T23:21:22Z", quantity: 2, total: 190, tip: 100, type: "tab")
            });
        }

        [Test]
        public void Top_ObservesTheAssociatedDimensionsFilters()
        {
            _quantity.FilterExact(4);
            CollectionAssert.AreEqual(new[]
            {
                new Order(date: "2011-11-14T21:18:48Z", quantity: 4, total: 270, tip: 0, type: "tab")
            }, _total.Top(3));

            _quantity.FilterAll();

            _date.FilterRange(new DateTime(2011, 11, 14, 19, 0, 0), new DateTime(2011, 11, 14, 20, 0, 0));
            CollectionAssert.AreEqual(new[]
            {
                new Order(date: "2011-11-14T19:30:44Z", quantity: 2, total: 90, tip: 0, type: "tab"),
                new Order(date: "2011-11-14T19:04:22Z", quantity: 2, total: 90, tip: 0, type: "tab"),
                new Order(date: "2011-11-14T19:00:31Z", quantity: 2, total: 190, tip: 100, type: "tab")
            }, _date.Top(10));

            _date.FilterRange(new DateTime(2011, 11, 14, 19, 0, 0), new DateTime(2011, 11, 14, 20, 0, 0)); // also comparable
            CollectionAssert.AreEqual(new[]
            {
                new Order(date: "2011-11-14T19:30:44Z", quantity: 2, total: 90, tip: 0, type: "tab"),
                new Order(date: "2011-11-14T19:04:22Z", quantity: 2, total: 90, tip: 0, type: "tab"),
                new Order(date: "2011-11-14T19:00:31Z", quantity: 2, total: 190, tip: 100, type: "tab")
            }, _date.Top(10));
        }

        [Test]
        public void Top_ObservesOtherDimensionsFilters()
        {
            _type.FilterExact("tab");
            CollectionAssert.AreEqual(_total.Top(2), new[]
            {
                new Order(date: "2011-11-14T20:49:07Z", quantity: 2, total: 290, tip: 200, type: "tab"),
                new Order(date: "2011-11-14T21:18:48Z", quantity: 4, total: 270, tip: 0, type: "tab")
            });
            _type.FilterExact("visa");
            CollectionAssert.AreEqual(_total.Top(1), new[]
            {
                new Order(date: "2011-11-14T16:28:54Z", quantity: 1, total: 300, tip: 200, type: "visa")
            });
            _quantity.FilterExact(2);
            CollectionAssert.AreEqual(_tip.Top(1), new[]
            {
                new Order(date: "2011-11-14T17:38:40Z", quantity: 2, total: 200, tip: 100, type: "visa")
            });

            _type.FilterAll();
            _quantity.FilterAll();

            _type.FilterExact("tab");
            CollectionAssert.AreEqual(_date.Top(2), new[]
            {
                new Order(date: "2011-11-14T23:28:54Z", quantity: 2, total: 190, tip: 100, type: "tab"),
                new Order(date: "2011-11-14T23:23:29Z", quantity: 2, total: 190, tip: 100, type: "tab")
            });
            _type.FilterExact("visa");
            CollectionAssert.AreEqual(_date.Top(1), new[]
            {
                new Order(date: "2011-11-14T23:16:09Z", quantity: 1, total: 200, tip: 100, type: "visa")
            });
            _quantity.FilterExact(2);
            CollectionAssert.AreEqual(_date.Top(1), new[]
            {
                new Order(date: "2011-11-14T22:58:54Z", quantity: 2, total: 100, tip: 0, type: "visa")
            });
        }

        [Test]
        public void Top_NegativeOrZeroKReturnsAnEmptyArray()
        {
            CollectionAssert.AreEqual(_quantity.Top(0), new Order[0]);
            CollectionAssert.AreEqual(_quantity.Top(-1), new Order[0]);
            CollectionAssert.AreEqual(_date.Top(0), new Order[0]);
            CollectionAssert.AreEqual(_date.Top(-1), new Order[0]);
        }

        [Test]
        public void Bottom_ReturnsTheBottomKRecordsByValueInDescendingOrder()
        {
            CollectionAssert.AreEqual(new[]
            {
                new Order(date: "2011-11-14T22:30:22Z", quantity: 2, total: 89, tip: 0, type: "tab"),
                new Order(date: "2011-11-14T16:30:43Z", quantity: 2, total: 90, tip: 0, type: "tab"),
                new Order(date: "2011-11-14T16:48:46Z", quantity: 2, total: 90, tip: 0, type: "tab"),
            }, _total.Bottom(3));

            CollectionAssert.AreEqual(new[]
            {
                new Order(date: "2011-11-14T16:17:54Z", quantity: 2, total: 190, tip: 100, type: "tab"),
                new Order(date: "2011-11-14T16:20:19Z", quantity: 2, total: 190, tip: 100, type: "tab"),
                new Order(date: "2011-11-14T16:28:54Z", quantity: 1, total: 300, tip: 200, type: "visa")
            }, _date.Bottom(3));
        }

        [Test]
        public void Bottom_ObservesTheAssociatedDimensionsFilters()
        {
            // TODO: Use stable sort!
            _quantity.FilterExact(4);
            CollectionAssert.AreEqual(_total.Bottom(3), new[]
            {
                new Order(date: "2011-11-14T21:18:48Z", quantity: 4, total: 270, tip: 0, type: "tab")
            });

            _quantity.FilterAll();
            
            _date.FilterRange(new DateTime(2011, 11, 14, 19, 0, 0), new DateTime(2011, 11, 14, 20, 0, 0));
            CollectionAssert.AreEqual(_date.Bottom(10), new[]
            {
                new Order(date: "2011-11-14T19:00:31Z", quantity: 2, total: 190, tip: 100, type: "tab"),
                new Order(date: "2011-11-14T19:04:22Z", quantity: 2, total: 90, tip: 0, type: "tab"),
                new Order(date: "2011-11-14T19:30:44Z", quantity: 2, total: 90, tip: 0, type: "tab")
            });

            _date.FilterRange(new DateTime(2011, 11, 14, 19, 0, 0), new DateTime(2011, 11, 14, 20, 0, 0)); // also comparable
            CollectionAssert.AreEqual(_date.Bottom(10), new[]
            {
                new Order(date: "2011-11-14T19:00:31Z", quantity: 2, total: 190, type: "tab", tip: 100),
                new Order(date: "2011-11-14T19:04:22Z", quantity: 2, total: 90, type: "tab", tip: 0),
                new Order(date: "2011-11-14T19:30:44Z", quantity: 2, total: 90, type: "tab", tip: 0)
            });
        }

        [Test]
        public void Bottom_ObservesOtherDimensionsFilters()
        {
            _type.FilterExact("tab");
            CollectionAssert.AreEqual(_total.Bottom(2), new[]
            {
                new Order(date: "2011-11-14T22:30:22Z", quantity: 2, total: 89, tip: 0, type: "tab"),
                new Order(date: "2011-11-14T16:30:43Z", quantity: 2, total: 90, tip: 0, type: "tab")
            });
            _type.FilterExact("visa");
            CollectionAssert.AreEqual(_total.Bottom(1), new[]
            {
                new Order(date: "2011-11-14T22:58:54Z", quantity: 2, total: 100, tip: 0, type: "visa")
            });
            _quantity.FilterExact(2);
            CollectionAssert.AreEqual(_tip.Bottom(1), new[]
            {
                new Order(date: "2011-11-14T22:58:54Z", quantity: 2, total: 100, tip: 0, type: "visa")
            });

            _type.FilterAll();
            _quantity.FilterAll();

            _type.FilterExact("tab");
            CollectionAssert.AreEqual(_date.Bottom(2), new[]
            {
                new Order(date: "2011-11-14T16:17:54Z", quantity: 2, total: 190, tip: 100, type: "tab"),
                new Order(date: "2011-11-14T16:20:19Z", quantity: 2, total: 190, tip: 100, type: "tab")
            });
            _type.FilterExact("visa");
            CollectionAssert.AreEqual(_date.Bottom(1), new[]
            {
                new Order(date: "2011-11-14T16:28:54Z", quantity: 1, total: 300, tip: 200, type: "visa")
            });
            _quantity.FilterExact(2);
            CollectionAssert.AreEqual(_date.Bottom(1), new[]
            {
                new Order(date: "2011-11-14T17:38:40Z", quantity: 2, total: 200, tip: 100, type: "visa")
            });
        }

        [Test]
        public void Bottom_NegativeOrZeroKReturnsAnEmptyArray()
        {
            CollectionAssert.AreEqual(_quantity.Bottom(0), new Order[0]);
            CollectionAssert.AreEqual(_quantity.Bottom(-1), new Order[0]);
            CollectionAssert.AreEqual(_date.Bottom(0), new Order[0]);
            CollectionAssert.AreEqual(_date.Bottom(-1), new Order[0]);
        }

        [Test]
        public void FilterExact_SelectsRecordThatMatchTheSpecifiedValueExactly()
        {
            _tip.FilterExact(100);
            CollectionAssert.AreEqual(_date.Top(2), new[]
            {
                new Order(date: "2011-11-14T23:28:54Z", quantity: 2, total: 190, tip: 100, type: "tab"),
                new Order(date: "2011-11-14T23:23:29Z", quantity: 2, total: 190, tip: 100, type: "tab")
            });
        }

        [Test]
        public void FilterExact_AllowsTheFilterValueToBeNull()
        {
            // TODO: Convert 0 to null
            _tip.FilterExact(0); // equivalent to 0 by natural ordering
            CollectionAssert.AreEqual(_date.Top(2), new[]
            {
                new Order(date: "2011-11-14T22:58:54Z", quantity: 2, total: 100, tip: 0, type: "visa"),
                new Order(date: "2011-11-14T22:48:05Z", quantity: 2, total: 91, tip: 0, type: "tab")
            });
        }

        [Test]
        public void FilterRange_SelectsRecordsGreaterOrEqualToTheInclusiveLowerBound()
        {
            _total.FilterRange(100, 190);
            Assert.IsTrue(_date.Top(int.MaxValue).All(d => d.Total >= 100));
            _total.FilterRange(110, 190);
            Assert.IsTrue(_date.Top(int.MaxValue).All(d => d.Total >= 110));
        }

        [Test]
        public void FilterRange_SelectsRecordsLessThanTheExpectedLowerBound()
        {
            _total.FilterRange(100, 200);
            Assert.IsTrue(_date.Top(int.MaxValue).All(d => d.Total < 200));
            _total.FilterRange(100, 190);
            Assert.IsTrue(_date.Top(int.MaxValue).All(d => d.Total < 190));
        }

        [Test]
        public void FilterAll_ClearsTheFilter()
        {
            _total.FilterRange(100, 200);
            Assert.IsTrue(_date.Top(int.MaxValue).Count < 43);
            _total.FilterAll();
            Assert.AreEqual(_date.Top(int.MaxValue).Count, 43);
        }

        [Test]
        public void FilterFunction_SelectsRecordsAccordingToAnArbitraryFunction()
        {
            _total.FilterFunction(d => d%2 > 0);
            Assert.IsTrue(_date.Top(int.MaxValue).All(d => d.Total%2 > 0));
        }

        [Test]
        public void FilterFunction_RespectsTruthyValues()
        {
            var group = _quantity.GroupAll().ReduceCount();
            _total.FilterRange(200, int.MaxValue);
            _total.FilterFunction(d => true);
            Assert.AreEqual(group.Value, 43);
            _total.FilterFunction(d => false);
            Assert.AreEqual(group.Value, 0);
        }
        
        [Test]
        public void FilterFunction_GroupsOnTheFirstDimensionAreUpdatedCorrectly()
        {
            var group = _date.GroupAll().ReduceCount();
            _total.FilterFunction(d => d == 90);
            Assert.AreEqual(group.Value, 13);
            _total.FilterFunction(d => d == 91);
            Assert.AreEqual(group.Value, 1);
        }

        [Test]
        public void FilterFunction_FollwedByFilterRange()
        {
            _total.FilterFunction(d => d%2 > 0);
            _total.FilterRange(100, 200);
            Assert.AreEqual(_date.Top(int.MaxValue).Count, 19);
        }

        [Test]
        public void Filter_IsEquivalentToFilterRangeWhenPassedAnArray()
        {
            _total.Filter(100, 190);
            Assert.IsTrue(_date.Top(int.MaxValue).All(d => d.Total >= 100));
        }

        [Test]
        public void Filter_IsEquivalentToFilterExactWhenPassedASingleValue()
        {
            _total.Filter(100);
            Assert.IsTrue(_date.Top(int.MaxValue).All(d => d.Total == 100));
        }

        [Test]
        public void Filter_IsEquivalentToFilterFunctionWhenPassedAFunction()
        {
            _total.Filter(d => d%2 > 0);
            Assert.IsTrue(_date.Top(int.MaxValue).All(d => d.Total%2 > 0));
        }

        [Test]
        public void Filer_IsEquivalentToFilterAllWhenPassedNull()
        {
            _total.Filter(100, 200);
            Assert.IsTrue(_date.Top(int.MaxValue).Count < 43);
            _total.Filter(null);
            Assert.AreEqual(_date.Top(int.MaxValue).Count, 43);
        }

        [Test]
        public void GroupAllCountDefault_DoesNotHaveTopAndOrderMethods()
        {
            var count = _quantity.GroupAll();

            Assert.IsNull(count.GetType().GetMethod("Top"));
            Assert.IsNull(count.GetType().GetMethod("Order"));
        }

        [Test]
        public void GroupAllCountDefault_Reduce_ReducesByAddRemoveAndInitial()
        {
            var count = _quantity.GroupAll();

            count.Reduce(
                (p, v) => p + v.Total,
                (p, v) => p - v.Total,
                () => 0);

            Assert.AreEqual(count.Value, 6660);
        }

        [Test]
        public void GroupAllCountDefault_ReduceCount_ReducesByCount()
        {
            var count = _quantity.GroupAll();

            count.ReduceSum(d => d.Total);
            Assert.AreEqual(count.Value, 6660);
            count.ReduceCount();
            Assert.AreEqual(count.Value, 43);
        }

        [Test]
        public void GroupAllCountDefault_ReduceSum_ReducesBySumOfAccessorFunction()
        {
            var count = _quantity.GroupAll();


            count.ReduceSum(d => d.Total);
            Assert.AreEqual(count.Value, 6660);
            count.ReduceSum(d => 1);
            Assert.AreEqual(count.Value, 43);
        }
        
        [Test]
        public void GroupAllCountDefault_Value_ReturnsTheCountOfMatchingRecords()
        {
            var count = _quantity.GroupAll();

            Assert.AreEqual(count.Value, 43);
        }

        [Test]
        public void GroupAllCountDefault_Value_DoesNotObserveTheAssociatedDimensionsFilters()
        {
            var count = _quantity.GroupAll();

            _quantity.FilterRange(100, 200);
            Assert.AreEqual(count.Value, 43);
        }

        [Test]
        public void GroupAllCountDefault_Value_ObservesOtherDimensionsFilters()
        {
            var count = _quantity.GroupAll();

            _type.FilterExact("tab");
            Assert.AreEqual(count.Value, 32);
            _type.FilterExact("visa");
            Assert.AreEqual(count.Value, 7);
            _tip.FilterExact(100);
            Assert.AreEqual(count.Value, 5);
        }

        [Test]
        public void GroupAllCountDefault_Dispose_DetachesFromReduceListeners()
        {
            var callback = false;
            var data = new XFilter<int>(new[] {0, 1, 2});
            var dimension = data.Dimension(d => d);
            var other = data.Dimension(d => d);
            var all = dimension.GroupAll().Reduce((v, p) =>
            {
                callback = true;
                return 0;
            }, (v, p) =>
            {
                callback = true;
                return 0;
            }, () => 0);
            var temp = all.Value; // force this group to be reduced when filters change
            all.Dispose();

            callback = false;
            other.FilterRange(1, 2);
            Assert.IsFalse(callback);
        }

        [Test]
        public void GroupAllCountDefault_Dispose_DetachesFromAddListeners()
        {
            var callback = false;
            var data = new XFilter<int>(new[] {0, 1, 2});
            var dimension = data.Dimension(d => d);
            var all = dimension.GroupAll().Reduce((v, p) =>
            {
                callback = true;
                return 0;
            }, (v, p) =>
            {
                callback = true;
                return 0;
            }, () => 0);
            var temp = all.Value; // force this group to be reduced when data is added
            callback = false;
            all.Dispose();

            callback = false;
            data.Add(new[] {3, 4, 5});
            Assert.IsFalse(callback);
        }


        [Test]
        public void GroupAllSumOfTotal_DoesNotHaveTopAndOrderMethods()
        {
            var total = _quantity.GroupAll().ReduceSum(d => d.Total);
            Assert.IsNull(total.GetType().GetMethod("Top"));
            Assert.IsNull(total.GetType().GetMethod("Order"));
        }

        [Test]
        public void GroupAllSumOfTotal_Reduce_DetermintesTheComputedReduceValue()
        {
            var total = _quantity.GroupAll().ReduceSum(d => d.Total);

            total.Reduce(
                (p, v) => p + 1,
                (p, v) => p - 1,
                () => 0);
            Assert.AreEqual(total.Value, 43);
        }

        [Test]
        public void GroupAllSumOfTotal_Value_ReturnsTheSumTotalOfMatchingRecords()
        {
            var total = _quantity.GroupAll().ReduceSum(d => d.Total);

            Assert.AreEqual(total.Value, 6660);
        }

        [Test]
        public void GroupAllSumOfTotal_Reduce_DoesNotObserveTheAssociatedDimensionsFilters()
        {
            var total = _quantity.GroupAll().ReduceSum(d => d.Total);

            _quantity.FilterRange(100, 200);
            Assert.AreEqual(total.Value, 6660);
        }

        [Test]
        public void GroupAllSumOfTotal_Reduce_ObservesOtherDimensionsFilters()
        {
            var total = _quantity.GroupAll().ReduceSum(d => d.Total);

            _type.FilterExact("tab");
            Assert.AreEqual(total.Value, 4760);
            _type.FilterExact("visa");
            Assert.AreEqual(total.Value, 1400);
            _tip.FilterExact(100);
            Assert.AreEqual(total.Value, 1000);
        }
        
        [Test]
        public void Group_KeyDefaultsToValue()
        {
            var types = _type.Group();

            CollectionAssert.AreEqual(new[]
            {
                new KeyValue<string>("tab", 32),
                new KeyValue<string>("visa", 7),
                new KeyValue<string>("cash", 4)
            }, types.Top(int.MaxValue));
        }

        [Test]
        public void Group_CardinalityMayBeGreaterThan256()
        {
            var data = new XFilter<int>(Enumerable.Range(0, 256).Concat(new[] {256, 256}).ToList());
            var index = data.Dimension(d => d);
            var indexes = index.Group();
            CollectionAssert.AreEqual(new[] {256, 256}, index.Top(2));
            CollectionAssert.AreEqual(indexes.Top(1), new[] {new KeyValue<int>(256, 2)});
            Assert.AreEqual(indexes.Count, 257);
        }

        [Test]
        public void Group_CardinalityMayBeGreaterThan65536()
        {
            var data = new XFilter<int>(Enumerable.Range(0, 65536).Concat(new[] {65536, 65536}).ToList());
            var index = data.Dimension(d => d);
            var indexes = index.Group();
            CollectionAssert.AreEqual(index.Top(2), new[] {65536, 65536});
            CollectionAssert.AreEqual(indexes.Top(1), new[] {new KeyValuePair<int, int>(65536, 2)});
            Assert.AreEqual(indexes.Count, 65537);
        }

        [Test]
        public void Group_Count_ReturnsTheCardinality()
        {
            var hours = _date.Group(d => d.Date.AddHours(d.Hour));
            var types = _type.Group();

            Assert.AreEqual(3, types.Count);
            Assert.AreEqual(8, hours.Count);
        }

        [Test]
        public void Group_Size_IgnoresAnyFilters()
        {
            var hours = _date.Group(d => d.Date.AddHours(d.Hour));
            var types = _type.Group();

            _type.FilterExact("tab");
            _quantity.FilterRange(100, 200);
            Assert.AreEqual(hours.Count, 8);
            Assert.AreEqual(types.Count, 3);
        }

        [Test]
        public void Group_Reduce_DefaultsToCount()
        {
            var hours = _date.Group(d => d.Date.AddHours(d.Hour));

            CollectionAssert.AreEqual(new[]
            {
                new KeyValue<DateTime>(new DateTime(2011, 11, 14, 17, 0, 0), 9)
            }, hours.Top(1));
        }

        [Test]
        public void Group_Reduce_DeterminesTheComputedReduceValue()
        {
            var hours = _date.Group(d => d.Date.AddHours(d.Hour));
            
            hours.ReduceSum(d => d.Total);
            CollectionAssert.AreEqual(hours.Top(1), new[]
            {
                new KeyValue<DateTime>(new DateTime(2011, 11, 14, 17, 0, 0), 1240)
            });
        }

        [Test]
        public void Group_Top_ReturnsTheTopKGroupsByReduceValueInDescendingOrder()
        {
            var hours = _date.Group(d => d.Date.AddHours(d.Hour));

            CollectionAssert.AreEqual(hours.Top(3), new[]
            {
                new KeyValue<DateTime>(new DateTime(2011, 11, 14, 17, 0, 0), 9),
                new KeyValue<DateTime>(new DateTime(2011, 11, 14, 16, 0, 0), 7),
                new KeyValue<DateTime>(new DateTime(2011, 11, 14, 21, 0, 0), 6)
            });
        }

        [Test]
        public void Group_ObservesTheSpecifiedOrder()
        {
            var hours = _date.Group(d => d.Date.AddHours(d.Hour));

            hours.Order(v => -v.Ticks);
            CollectionAssert.AreEqual(new[]
            {
                new KeyValue<DateTime>(new DateTime(2011, 11, 14, 20, 00, 00), 2),
                new KeyValue<DateTime>(new DateTime(2011, 11, 14, 19, 00, 00), 3),
                new KeyValue<DateTime>(new DateTime(2011, 11, 14, 18, 00, 00), 5)
            }, hours.Top(3));
        }

        [Test]
        public void Group_Order_DefaultsToIdentityFunction()
        {
            var hours = _date.Group(d => d.Date.AddHours(d.Hour));

            CollectionAssert.AreEqual(hours.Top(1), new[]
            {
                new KeyValue<DateTime>(new DateTime(2011, 11, 14, 17, 0, 0), 9)
            });
        }

        [Test]
        public void Group_Order_IsUsefulInConjunctionWithACompoundReduceValue()
        {
            var hours = _date.Group(d => d.Date.AddHours(d.Hour));

            //// TODO: Figure out API
            //hours.Reduce(
            //    (p, v) =>
            //    {
            //        ++p.Count;
            //        p.Total += v.Total;
            //        return p;
            //    },
            //    (p, v) =>
            //    {
            //        --p.Count;
            //        p.Total -= v.Total;
            //        return p;
            //    },
            //    () => new Compound())
            //    .Order(v => v.Total);

            CollectionAssert.AreEqual(hours.Top(1), new[]
            {
                new KeyValuePair<DateTime, Compound>(new DateTime(2011, 11, 14, 17, 0, 0), new Compound {Count = 9, Total = 1240})
            });
        }

        internal class Compound
        {
            public int Count;
            public int Total;

            public override bool Equals(object obj)
            {
                var other = obj as Compound;
                return other != null
                       && other.Count == Count
                       && other.Total == Total;
            }
        }


        [Test]
        public void Group_Dispose_DetachesFromReduceListeners()
        {
            var data = new XFilter<int>(new[] {0, 1, 2});
            var callback = false; // indicates a reduce has occurred in this group
            var dimension = data.Dimension(d => d);
            var other = data.Dimension(d => d);
            var group = dimension
                .Group(d => d)
                .Reduce((p, v) =>
                {
                    callback = true;
                    return 0;
                }, (p, v) =>
                {
                    callback = true;
                    return 0;
                }, () => 0);

            group.All(); // force this group to be reduced when filters change
            callback = false;
            group.Dispose();
            other.FilterRange(1, 2);
            Assert.IsFalse(callback);
        }

        [Test]
        public void Group_Detaches_FromAddListeners()
        {
            var data = new XFilter<int>(new[] {0, 1, 2});
            var callback = false; // indicates a reduce has occurred in this group
            var dimension = data.Dimension(d => d);
            var group = dimension
                .Group(d => d)
                .Reduce((p, v) =>
                {
                    callback = true;
                    return 0;
                }, (p, v) =>
                {
                    callback = true;
                    return 0;
                }, () => 0);

            group.All(); // force this group to be reduced when filters change
            callback = false;
            group.Dispose();
            data.Add(new[] {3, 4, 5});
            Assert.IsFalse(callback);
        }

        [Test]
        public void Dispose_DetachesFromAddListeners()
        {
            var data = new XFilter<int>(new[] {0, 1, 2});
            var callback = false; // indicates a reduce has occurred in this group
            var dimension = data.Dimension(d =>
            {
                callback = true;
                return d;
            });

            callback = false;
            dimension.Dispose();
            data.Add(new[] {3, 4, 5});
            Assert.IsFalse(callback);
        }

        [Test]
        public void Dispose_DetachesGroupsFromReduceListeners()
        {
            var data = new XFilter<int>(new[] {0, 1, 2});
            var callback = false; // indicates a reduce has occurred in this group
            var dimension = data.Dimension(d => d);
            var other = data.Dimension(d => d);
            var group = dimension
                .Group(d => d)
                .Reduce(
                    (p, v) =>
                    {
                        callback = true;
                        return 0;
                    },
                    (p, v) =>
                    {
                        callback = true;
                        return 0;
                    }, () => 0);
            group.All(); // force this group to be reduced when filters change
            callback = false;
            dimension.Dispose();
            other.FilterRange(1, 2);
            Assert.IsFalse(callback);
        }

        [Test]
        public void Dispose_DetachesGroupsFromAddListeners()
        {

            var data = new XFilter<int>(new[] {0, 1, 2});
            var callback = false;
            var dimension = data.Dimension(d => d);
            var group = dimension
                .Group(d => d)
                .Reduce(
                    (p, v) =>
                    {
                        callback = true;
                        return 0;
                    },
                    (p, v) =>
                    {
                        callback = true;
                        return 0;
                    }, () => 0);
            group.All(); // force this group to be reduced when filters change
            callback = false;
            dimension.Dispose();
            data.Add(new[] {3, 4, 5});
            Assert.IsFalse(callback);
        }

        [Test]
        public void Dispose_ClearsDimensionFiltersFromGroups()
        {

            var data = new XFilter<int>(new[] {0, 0, 2, 2});
            var d1 = data.Dimension(d => -d);
            var d2 = data.Dimension(d => +d);
            var g2 = d2.Group(d => Math.Round((double) d/2)*2);
            d1.FilterRange(-1, 1); // a filter is present when the dimension is disposed
            d1.Dispose();
            CollectionAssert.AreEqual(new[]
            {
                new KeyValue<double>(0, 2),
                new KeyValue<double>(2, 2)
            }, g2.All());
        }
    }
}