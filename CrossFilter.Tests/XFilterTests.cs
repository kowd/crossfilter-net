using System;
using NUnit.Framework;

namespace CrossFilter.Tests
{
    [TestFixture]
    public abstract class XFilterTests
    {
        protected XFilter<Order> _data;
        protected XFilter<Order>.XDimension<DateTime> _date;
        protected XFilter<Order>.XDimension<int> _quantity;
        protected XFilter<Order>.XDimension<int> _tip;
        protected XFilter<Order>.XDimension<int> _total;
        protected XFilter<Order>.XDimension<string> _type;

        [SetUp]
        public virtual void Setup()
        {
            _data = GetData();

            _quantity = _data.Dimension(d => d.Quantity);
            _date = _data.Dimension(d => d.Date);
            _tip = _data.Dimension(d => d.Tip.GetValueOrDefault());
            _total = _data.Dimension(d => d.Total);
            _type = _data.Dimension(d => d.Type);
        }

        protected virtual XFilter<Order> GetData()
        {
            return new XFilter<Order>(new[]
            {
                new Order {Date = DateTime.Parse("2011-11-14T16:17:54Z"), Quantity = 2, Total = 190, Tip = 100, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T16:20:19Z"), Quantity = 2, Total = 190, Tip = 100, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T16:28:54Z"), Quantity = 1, Total = 300, Tip = 200, Type = "visa"},
                new Order {Date = DateTime.Parse("2011-11-14T16:30:43Z"), Quantity = 2, Total = 90, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T16:48:46Z"), Quantity = 2, Total = 90, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T16:53:41Z"), Quantity = 2, Total = 90, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T16:54:06Z"), Quantity = 1, Total = 100, Tip = null, Type = "cash"},
                new Order {Date = DateTime.Parse("2011-11-14T17:02:03Z"), Quantity = 2, Total = 90, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T17:07:21Z"), Quantity = 2, Total = 90, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T17:22:59Z"), Quantity = 2, Total = 90, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T17:25:45Z"), Quantity = 2, Total = 200, Tip = null, Type = "cash"},
                new Order {Date = DateTime.Parse("2011-11-14T17:29:52Z"), Quantity = 1, Total = 200, Tip = 100, Type = "visa"},
                new Order {Date = DateTime.Parse("2011-11-14T17:33:46Z"), Quantity = 2, Total = 190, Tip = 100, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T17:33:59Z"), Quantity = 2, Total = 90, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T17:38:40Z"), Quantity = 2, Total = 200, Tip = 100, Type = "visa"},
                new Order {Date = DateTime.Parse("2011-11-14T17:52:02Z"), Quantity = 2, Total = 90, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T18:02:42Z"), Quantity = 2, Total = 190, Tip = 100, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T18:02:51Z"), Quantity = 2, Total = 190, Tip = 100, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T18:12:54Z"), Quantity = 1, Total = 200, Tip = 100, Type = "visa"},
                new Order {Date = DateTime.Parse("2011-11-14T18:14:53Z"), Quantity = 2, Total = 100, Tip = null, Type = "cash"},
                new Order {Date = DateTime.Parse("2011-11-14T18:45:24Z"), Quantity = 2, Total = 90, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T19:00:31Z"), Quantity = 2, Total = 190, Tip = 100, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T19:04:22Z"), Quantity = 2, Total = 90, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T19:30:44Z"), Quantity = 2, Total = 90, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T20:06:33Z"), Quantity = 1, Total = 100, Tip = null, Type = "cash"},
                new Order {Date = DateTime.Parse("2011-11-14T20:49:07Z"), Quantity = 2, Total = 290, Tip = 200, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T21:05:36Z"), Quantity = 2, Total = 90, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T21:18:48Z"), Quantity = 4, Total = 270, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T21:22:31Z"), Quantity = 1, Total = 200, Tip = 100, Type = "visa"},
                new Order {Date = DateTime.Parse("2011-11-14T21:26:30Z"), Quantity = 2, Total = 190, Tip = 100, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T21:30:55Z"), Quantity = 2, Total = 190, Tip = 100, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T21:31:05Z"), Quantity = 2, Total = 90, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T22:30:22Z"), Quantity = 2, Total = 89, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T22:34:28Z"), Quantity = 2, Total = 190, Tip = 100, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T22:48:05Z"), Quantity = 2, Total = 91, Tip = 0, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T22:51:40Z"), Quantity = 2, Total = 190, Tip = 100, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T22:58:54Z"), Quantity = 2, Total = 100, Tip = 0, Type = "visa"},
                new Order {Date = DateTime.Parse("2011-11-14T23:06:25Z"), Quantity = 2, Total = 190, Tip = 100, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T23:07:58Z"), Quantity = 2, Total = 190, Tip = 100, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T23:16:09Z"), Quantity = 1, Total = 200, Tip = 100, Type = "visa"},
                new Order {Date = DateTime.Parse("2011-11-14T23:21:22Z"), Quantity = 2, Total = 190, Tip = 100, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T23:23:29Z"), Quantity = 2, Total = 190, Tip = 100, Type = "tab"},
                new Order {Date = DateTime.Parse("2011-11-14T23:28:54Z"), Quantity = 2, Total = 190, Tip = 100, Type = "tab"}
            });
        }
    }

    public class Order
    {

        public Order()
        {
        }

        public Order(string date, int quantity, int total, int tip, string type)
        {
            Date = DateTime.Parse(date);
            Quantity = quantity;
            Total = total;
            Tip = tip;
            Type = type;
        }

        public DateTime Date { get; set; }
        public int Quantity { get; set; }
        public int Total { get; set; }
        public int? Tip { get; set; }
        public string Type { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as Order;
            if (other != null)
            {
                return other.Date == Date
                       && other.Quantity == Quantity
                       && other.Tip == Tip
                       && other.Total == Total
                       && string.Equals(other.Type, Type);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Date.GetHashCode()
                   ^ Quantity
                   ^ Total
                   ^ Tip.GetHashCode()
                   ^ (Type ?? "").GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("Date:{0} Quantity:{1} Total:{2} Tip:{3} Type:{4}", Date, Quantity, Total, Tip, Type);
        }
    }
}