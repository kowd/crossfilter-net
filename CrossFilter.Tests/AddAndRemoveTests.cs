using System;
using System.Linq;
using NUnit.Framework;

namespace CrossFilter.Tests
{
    [TestFixture]
    public class AddAndRemoveTests
    {
        [Test]
        public void Add_IncreasesTheSizeOfTheCrossfilter()
        {
            var data = new XFilter<int>(new int[0]);
            Assert.AreEqual(data.Count, 0);
            data.Add(new[] {0, 1, 2, 3, 4, 5, 6, 6, 6, 7});
            Assert.AreEqual(data.Count, 10);
            data.Add(new int[0]);
            Assert.AreEqual(data.Count, 10);
        }

        [Test]
        public void Add_ExistsFiltersAreConsistentWithNewRecords()
        {
            var data = new XFilter<int>(new int[0]);
            var foo = data.Dimension(d => +d);
            var bar = data.Dimension(d => -d);
            CollectionAssert.AreEqual(foo.Top(int.MaxValue), new Int16[0]);
            foo.FilterExact(42);
            data.Add(new[] {43, 42, 41});
            CollectionAssert.AreEqual(foo.Top(int.MaxValue), new[] {42});
            CollectionAssert.AreEqual(bar.Top(int.MaxValue), new[] {42});
            data.Add(new[] {43, 42});
            CollectionAssert.AreEqual(foo.Top(int.MaxValue), new[] {42, 42});
            CollectionAssert.AreEqual(bar.Top(int.MaxValue), new[] {42, 42});
            foo.FilterRange(42, 44);
            data.Add(new[] {43});
            CollectionAssert.AreEqual(foo.Top(int.MaxValue), new[] {43, 43, 43, 42, 42});
            CollectionAssert.AreEqual(bar.Top(int.MaxValue), new[] {42, 42, 43, 43, 43});
            foo.FilterFunction(d => d%2 == 1);
            data.Add(new[] {44, 44, 45});
            CollectionAssert.AreEqual(foo.Top(int.MaxValue), new[] {45, 43, 43, 43, 41});
            CollectionAssert.AreEqual(bar.Top(int.MaxValue), new[] {41, 43, 43, 43, 45});
            bar.FilterExact(-43);
            CollectionAssert.AreEqual(bar.Top(int.MaxValue), new[] {43, 43, 43});
            data.Add(new[] {43});
            CollectionAssert.AreEqual(bar.Top(int.MaxValue), new[] {43, 43, 43, 43});
            bar.FilterAll();
            data.Add(new[] {0});
            CollectionAssert.AreEqual(bar.Top(int.MaxValue), new[] {41, 43, 43, 43, 43, 45});
            foo.FilterAll();
            CollectionAssert.AreEqual(bar.Top(int.MaxValue), new[] {0, 41, 42, 42, 43, 43, 43, 43, 44, 44, 45});
        }

        [Test]
        public void Add_ExistingGroupsAreConsistentWithNewRecords()
        {
            var data = new XFilter<int>(new int[0]);
            var foo = data.Dimension(d => +d);
            var bar = data.Dimension(d => -d);
            var foos = foo.Group();
            var all = data.GroupAll();
            Assert.AreEqual(all.Value, 0);
            CollectionAssert.AreEqual(foos.All(), new KeyValue<int>[0]);
            foo.FilterExact(42);
            data.Add(new[] {43, 42, 41});
            Assert.AreEqual(all.Value, 1);
            CollectionAssert.AreEqual(foos.All(),
                new[] {new KeyValue<int>(41, 1), new KeyValue<int>(42, 1), new KeyValue<int>(43, 1)});
            bar.FilterExact(-42);
            Assert.AreEqual(all.Value, 1);
            /*
                CollectionAssert.AreEqual(foos.all(), [{ key: 41, value: 0 }, { key: 42, value: 1 }, { key: 43, value: 0 }]);
                _add([43, 42, 41]);
                Assert.AreEqual(all.Value, 2);
                CollectionAssert.AreEqual(foos.all(), [{ key: 41, value: 0 }, { key: 42, value: 2 }, { key: 43, value: 0 }]);
                bar.FilterAll();
                Assert.AreEqual(all.Value, 2);
                CollectionAssert.AreEqual(foos.all(), [{ key: 41, value: 2 }, { key: 42, value: 2 }, { key: 43, value: 2 }]);
                foo.FilterAll();
                Assert.AreEqual(all.Value, 6);
             */
        }

        [Test]
        public void Add_CanAddNewGroupsThatAreBeforeExistingGroups()
        {
            /*
             var data = crossfilter(),
                    foo = _dimension(d =>  +d; }),
                    foos = foo.group().reduce(add, remove, initial).order(order);
                _add([2]).add([1, 1, 1]);
                CollectionAssert.AreEqual(foos.Top(2), [{ key: 1, value: { foo: 3 } }, { key: 2, value: { foo: 1 } }]);
                function order(p) { return p.foo; }
                function add(p, v) { ++p.foo; return p; }
                function remove(p, v) { --p.foo; return p; }
                function initial() { return { foo: 0 }; }
             */
        }

        [Test]
        public void Add_CanAddMoreThan256Groups()
        {
            var data = new XFilter<int>(new int[0]);
            var foo = data.Dimension(d => +d);
            var bar = data.Dimension(d => +d);
            var foos = foo.Group();
            data.Add(Enumerable.Range(0, 256).ToList());
            CollectionAssert.AreEqual(foos.All().Select(d => d.Key).ToArray(), Enumerable.Range(0, 256).ToArray());
            Assert.That(foos.All().All(d => d.Value == 1));
            data.Add(new []{128});
            CollectionAssert.AreEqual(foos.Top(1), new[] {new KeyValue<int>(128, 2)});
            bar.FilterExact(0);
            data.Add(Enumerable.Range(-256, 256).ToArray());
            CollectionAssert.AreEqual(foos.All().Select(d => d.Key).ToArray(), Enumerable.Range(-256, 512));
            CollectionAssert.AreEqual(foos.Top(1), new[] {new KeyValue<int>(0, 1)});
        }

        [Test]
        public void Add_CanAddLotsOfGroupsInReverseOrder()
        {
            /*
              {
                var data = crossfilter(),
                    foo = _dimension(d =>  -d.foo; }),
                    bar = _dimension(d =>  d.bar; }),
                    foos = foo.group(Math.floor).reduceSum(d =>  d.foo; });
                bar.FilterExact1);
                for (var i = 0; i < 1000; i++) {
                    _add(d3.range(10).map(function (d) {
                        return { foo: i + d / 10, bar: i % 4, baz: d + i * 10 };
                    }));
                }
                CollectionAssert.AreEqual(foos.Top(1), [{ key: -998, value: 8977.5 }]);
            }
             */
        }

        [Test]
        public void Remove_RemovingARecordWorksForAGroupWithCardinalityOne()
        {
            var data = new XFilter<FooObj>();
            var foo = data.Dimension(d => d.Foo);
            var div2 = foo.Group(value => Math.Floor(value/2));
            var positive = foo.Group(value => value > 0);

            /*
              _add([{ foo: 1 }, { foo: 1.1 }, { foo: 1.2 }]);
                _foo.Filter(1.1);
                _remove();
                _foo.FilterAll();
                data.remove();
                CollectionAssert.AreEqual(_foo.Top(Infinity), []);
             */
        }

        [Test]
        public void Remove_RemovingARecordWorksForAnotherGroupWithCardonality()
        {
            var data = new XFilter<FooObj>();
            var foo = data.Dimension(d => d.Foo);
            var div2 = foo.Group(value => Math.Floor(value/2));
            var positive = foo.Group(value => value > 0);

            /*
             _add([{ foo: 0 }, { foo: -1 }]);
                CollectionAssert.AreEqual(_foo.positive.all(), [{ key: 0, value: 2 }]);
                _foo.Filter(0);
                _remove();
                CollectionAssert.AreEqual(_foo.positive.all(), [{ key: 0, value: 1 }]);
                _foo.FilterAll();
                CollectionAssert.AreEqual(_foo.Top(Infinity), [{ foo: -1 }]);
                _remove();
                CollectionAssert.AreEqual(_foo.Top(Infinity), []);
             */
        }

        [Test]
        public void Remove_RemovingARecordUpdatesDimension()
        {
            var data = new XFilter<FooObj>();
            var foo = data.Dimension(d => d.Foo);
            var div2 = foo.Group(value => Math.Floor(value/2));
            var positive = foo.Group(value => value > 0);

            /*
              _add([{ foo: 1 }, { foo: 2 }]);
                _foo.FilterExact1);
                _remove();
                _foo.FilterAll();
                CollectionAssert.AreEqual(_foo.Top(Infinity), [{ foo: 2 }]);
                _remove();
                CollectionAssert.AreEqual(_foo.Top(Infinity), []);
             */
        }

        [Test]
        public void Remove_RemovingARecordUpdatesGroup()
        {
            var data = new XFilter<FooObj>();
            var foo = data.Dimension(d => d.Foo);
            var div2 = foo.Group(value => Math.Floor(value/2));
            var positive = foo.Group(value => value > 0);

            /*
              _add([{ foo: 1 }, { foo: 2 }, { foo: 3 }]);
                CollectionAssert.AreEqual(_foo.Top(Infinity), [{ foo: 3 }, { foo: 2 }, { foo: 1 }]);
                CollectionAssert.AreEqual(_foo.div2.all(), [{ key: 0, value: 1 }, { key: 1, value: 2 }]);
                _foo.FilterRange([1, 3]);
                _remove();
                _foo.FilterAll();
                CollectionAssert.AreEqual(_foo.Top(Infinity), [{ foo: 3 }]);
                CollectionAssert.AreEqual(_foo.div2.all(), [{ key: 1, value: 1 }]);
                _remove();
                CollectionAssert.AreEqual(_foo.Top(Infinity), []);
                CollectionAssert.AreEqual(_foo.div2.all(), []);
             */
        }

        [Test]
        public void Remove_FilteringWorksCorrecrtlyAfterRemovingARecord()
        {
            var data = new XFilter<FooObj>();
            var foo = data.Dimension(d => d.Foo);
            var div2 = foo.Group(value => Math.Floor(value/2));
            var positive = foo.Group(value => value > 0);

            /*
             _add([{ foo: 1 }, { foo: 2 }, { foo: 3 }]);
                _foo.Filter(2);
                _remove();
                _foo.FilterAll();
                CollectionAssert.AreEqual(_foo.Top(Infinity), [{ foo: 3 }, { foo: 1 }]);
                _remove();
                CollectionAssert.AreEqual(_foo.Top(Infinity), []);
             */
        }

        internal struct FooObj
        {
            public FooObj(double value)
            {
                Foo = value;
            }

            public double Foo;
        }
    }
}