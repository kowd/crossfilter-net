using NUnit.Framework;

namespace CrossFilter.Tests
{
    [TestFixture]
    public class GroupAllTests : XFilterTests
    {
        [Test]
        public void DoesNotHaveTopAndOrderMethods()
        {
            var all = _data.GroupAll().ReduceSum(d => d.Total);

            Assert.IsNull(all.GetType().GetMethod("Top"));
            Assert.IsNull(all.GetType().GetMethod("Order"));
        }

        [Test]
        public void Reduce_DeterminesTheComputedReduceValue()
        {
            var all = _data.GroupAll().ReduceSum(d => d.Total);

            all.ReduceCount();
            Assert.AreEqual(all.Value, 43);
        }

        [Test]
        public void Value_ReturnsTheSumTotalOfMatchingRecords()
        {
            var all = _data.GroupAll().ReduceSum(d => d.Total);

            Assert.AreEqual(all.Value, 6660);
        }

            [Test]
            public void Value_ObservesAllDimensionsFilters()
            {
                var all = _data.GroupAll().ReduceSum(d => d.Total);

                _type.FilterExact("tab");
                Assert.AreEqual(all.Value, 4760);
                _type.FilterExact("visa");
                Assert.AreEqual(all.Value, 1400);
                _tip.FilterExact(100);
                Assert.AreEqual(all.Value, 1000);
            }

        [Test]
        public void Dispose_DetachesFromReduceListeners()
        {
            var data = new XFilter<int>(new[] {0, 1, 2});
            var callback = false; // indicates a reduce has occurred in this group
            var other = data.Dimension(d => d);
            var all = data.GroupAll().Reduce((p, v) =>
            {
                callback = true;
                return 0;
            }, (p, v) =>
            {
                callback = true;
                return 0;
            }, () => 0);
            var temp = all.Value; // force this group to be reduced when filters change
            callback = false;
            all.Dispose();
            other.FilterRange(1, 2);
            Assert.IsFalse(callback);
        }

        [Test]
        public void Dispose_DetachesFromAddListeners()
        {
            var data = new XFilter<int>(new[] {0, 1, 2});
            var callback = false; // indicates a reduce has occurred in this group
            var other = data.Dimension(d => d);
            var all = data.GroupAll().Reduce((p, v) =>
            {
                callback = true;
                return 0;
            }, (p, v) =>
            {
                callback = true;
                return 0;
            }, () => 0);
            var temp = all.Value; // force this group to be reduced when filters change
            callback = false;
            all.Dispose();
            data.Add(new[] {3, 4, 5});
            Assert.IsFalse(callback);
        }
    }
}