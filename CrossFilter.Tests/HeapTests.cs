﻿using NUnit.Framework;

namespace CrossFilter.Tests
{
    [TestFixture]
    public class HeapTests
    {
        [Test]
        public void ChildrenAreGreaterThanOrEqualToParents()
        {
            var array = new[] {6, 5, 3, 1, 8, 7, 2, 4};
            var n = array.Length;
            Assert.AreSame(Heap.It(array, 0, n), array);
            Assert.AreEqual(array[0], 1);
            for (var i = 1; i < n; ++i)
            {
                Assert.IsTrue(array[i] >= array[i - 1 >> 1]);
            }
        }

        [Test]
        public void CreatesAHeapFromASubsetOfTheArray()
        {
            var array = new[] {6, 5, 3, 1, 8, 7, 2, 4};
            var n = 6;
            Assert.AreSame(Heap.It(array, 0, n), array);
            Assert.AreEqual(array[0], 1);
            for (var i = 1; i < n; ++i)
            {
                Assert.IsTrue(array[i] >= array[i - 1 >> 1]);
            }
        }

        [Test]
        public void SortsAnExistingHeapInDescendingOrder()
        {
            var array = new[] {1, 4, 2, 5, 8, 7, 3, 6};
            var n = array.Length;
            Heap.Sort(array, 0, n);
            CollectionAssert.AreEqual(array, new[] {8, 7, 6, 5, 4, 3, 2, 1});
        }

        [Test]
        public void SortsATwoElementHeapInDescendingOrder()
        {
            var array = new[] {1, 4};
            var n = array.Length;
            Heap.Sort(array, 0, n);
            CollectionAssert.AreEqual(array, new[] {4, 1});
        }
    }
}