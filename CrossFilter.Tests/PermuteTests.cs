﻿using NUnit.Framework;

namespace CrossFilter.Tests
{
    [TestFixture]
    public class MyTestClass
    {
        [Test]
        public void PermutesAccordingToTheSpecifiedIndex()
        {
            CollectionAssert.AreEqual(Permute.It(new[] { 3, 4, 5 }, new[] { 2, 1, 0 }), new[] { 5, 4, 3 });
            CollectionAssert.AreEqual(Permute.It(new[] { 3, 4, 5 }, new[] { 2, 0, 1 }), new[] { 5, 3, 4 });
            CollectionAssert.AreEqual(Permute.It(new[] { 3, 4, 5 }, new[] { 0, 1, 2 }), new[] { 3, 4, 5 });
        }

        [Test]
        public void DoesNotModifyTheInputArray()
        {
            var input = new[] {3, 4, 5};
            Permute.It(input, new[] { 2, 1, 0 });
            CollectionAssert.AreEqual(input, new[] {3, 4, 5});
        }

        [Test]
        public void CanDuplicateInputValues()
        {
            CollectionAssert.AreEqual(Permute.It(new[] { 3, 4, 5 }, new[] { 0, 1, 0 }), new[] { 3, 4, 3 });
            CollectionAssert.AreEqual(Permute.It(new[] { 3, 4, 5 }, new[] { 2, 2, 2 }), new[] { 5, 5, 5 });
            CollectionAssert.AreEqual(Permute.It(new[] { 3, 4, 5 }, new[] { 0, 1, 1 }), new[] { 3, 4, 4 });
        }

        [Test]
        public void CanReturnMoreElements()
        {
            CollectionAssert.AreEqual(Permute.It(new[] { 3, 4, 5 }, new[] { 0, 0, 1, 2 }), new[] { 3, 3, 4, 5 });
            CollectionAssert.AreEqual(Permute.It(new[] { 3, 4, 5 }, new[] { 0, 1, 1, 1 }), new[] { 3, 4, 4, 4 });
        }

        [Test]
        public void CanReturnFewerElements()
        {
            CollectionAssert.AreEqual(Permute.It(new[] { 3, 4, 5 }, new[] { 0 }), new[] { 3 });
            CollectionAssert.AreEqual(Permute.It(new[] { 3, 4, 5 }, new[] { 1, 2 }), new[] { 4, 5 });
            CollectionAssert.AreEqual(Permute.It(new[] { 3, 4, 5 }, new int[] { }), new int[] { });
        }

        [Test]
        public void CanReturnDefaultElements()
        {
            var v1 = Permute.It(new[] {3, 4, 5}, new[] {10});
            Assert.AreEqual(v1.Length, 1);
            Assert.AreEqual(0, v1[0]);
            
            var v2 = Permute.It(new[] {3, 4, 5}, new[] {-1});
            Assert.AreEqual(v2.Length, 1);
            Assert.AreEqual(0, v2[0]);
            
            var v3 = Permute.It(new[] {3, 4, 5}, new[] {0, -1});
            Assert.AreEqual(v3.Length, 2);
            Assert.AreEqual(v3[0], 3);
            Assert.AreEqual(0, v3[1]);
        }
    }
}