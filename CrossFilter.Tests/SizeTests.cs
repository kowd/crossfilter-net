using NUnit.Framework;

namespace CrossFilter.Tests
{
    [TestFixture]
    public class SizeTests : XFilterTests
    {
        [Test]
        public void ReturnsTheTotalNumberOfElements()
        {
            Assert.AreEqual(_data.Count, 43);
        }

        [Test]
        public void IsNotAffectedByAnyDimensionFilters()
        {
            _quantity.FilterExact(4);

            Assert.AreEqual(_data.Count, 43);
        }
    }
}