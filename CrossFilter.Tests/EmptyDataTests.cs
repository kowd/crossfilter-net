using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace CrossFilter.Tests
{
    [TestFixture]
    public class EmptyDataTests : XFilterTests
    {
        protected override XFilter<Order> GetData()
        {
            return new XFilter<Order>();
        }

        [Test]
        public void UpTo32DimensionsSupported()
        {
            var data = new XFilter<int>(new int[0]);
            var thrown = false;
            for (var i = 0; i < 32; i++)
            {
                data.Dimension(n => 0);
            }

            try
            {
                data.Dimension(n => 0);
            }
            catch (InvalidOperationException e)
            {
                thrown = true;
            }

            Assert.IsTrue(thrown);
        }

        [Test]
        public void CanAddAndRemove32DimensionsRepeatedly()
        {
            var data = new XFilter<Order>(new Order[0]);
            var dimensions = new Stack<XFilter<Order>.XDimension<int>>();
            for (var j = 0; j < 10; j++)
            {
                for (var i = 0; i < 32; i++)
                {
                    dimensions.Push(data.Dimension(n => 0));
                }

                while (dimensions.Any())
                {
                    dimensions.Pop().Dispose();
                }
            }
        }

        [Test]
        public void GroupAllValue()
        {
            var all = _data.GroupAll();

            Assert.AreEqual(0, all.Value);
        }

        [Test]
        public void GroupAllValueAfterRemovingAllData()
        {
            var all = _data.GroupAll();

            _data.Add(new[] {new Order {Quantity = 2, Total = 190}});
            Assert.AreEqual(1, all.Value);

            _data.Remove();
            Assert.AreEqual(0 ,all.Value);
        }

        
        [Test]
        public void Dimension_GroupAllCountDefault_Value()
        {
            var count = _quantity.GroupAll();

            Assert.AreEqual(0, count.Value);
        }

        [Test]
        public void Dimension_GroupAllCountDefault_ValueAfterRemovingAllData()
        {
            var count = _quantity.GroupAll();

            _data.Add(new[] {new Order {Quantity = 2, Total = 190}});
            Assert.AreEqual(1, count.Value);

            _data.Remove();
            Assert.AreEqual(0, count.Value);
        }

        [Test]
        public void Dimension_GroupAllSumOfTotal_Value()
        {
            var total = _quantity.GroupAll().ReduceSum(o => o.Total);

            Assert.AreEqual(0, total.Value);
        }

        [Test]
        public void Dimension_GroupAllSumOfTotal_ValueAfterRemovingAllData()
        {
            var total = _quantity.GroupAll().ReduceSum(o => o.Total);

            _data.Add(new[] {new Order {Quantity = 2, Total = 190}});
            Assert.AreEqual(total.Value, 190);

            _data.Remove();
            Assert.AreEqual(0, total.Value);
        }

        [Test]
        public void Dimension_GroupAllCustomReduce_Value()
        {
            var custom = _quantity.GroupAll().Reduce((p, v) => p + 1, (p, v) => p - 1, () => 1);

            Assert.AreEqual(1, custom.Value);
        }

        [Test]
        public void Dimension_GroupAllCustomReduce_ValueAfterRemovingAllData()
        {
            var custom = _quantity.GroupAll().Reduce((p, v) => p + 1, (p, v) => p - 1, () => 1);

            _data.Add(new[] {new Order {Quantity = 2, Total = 190}});
            Assert.AreEqual(2, custom.Value);
            
            _data.Remove();
            Assert.AreEqual(1, custom.Value);
        }
    }
}